Hardware counters with multiplexing
-----------------------------------

:Author: Guyon David
:Date: 05/21/2014

I made a test program and 4 programs using counters to get used to the multiplexing. They are located in *workaround-multiplex-counters/*:

 * ``test-program-decimal-sum`` is my test program on which I make my performance mesures
 * ``instructions-counter`` counts the number of instructions of the test program
 * ``multiplex-counters`` reads 13 counters with multiplexing
 * ``not-multiplex-counters`` does the same with no multiplexing
 * ``multiplex-counters-whole-system`` reads 4 counters by CPU during 2 seconds on the whole system

instructions-counter
~~~~~~~~~~~~~~~~~~~~

Here is the output of its execution:

.. code:: sh

    Calculate the number of instructions of the program 'test-program-decimal-sum'
    [i] CPU: -1
    [i] PID: 21494
    [i] GRP: -1
    [i] FLAGS :0
    [i] Child process
    Calculate the sum from 1 to 1000000000
    The sum from 1 to 1000000000 is 500000000500000000
    [i] Program executed with success!
    [i] The program executed in 6004579795 instructions

As you can see, it tells us that the program needed 6 004 579 795 instructions to execute. 

multiplex-counters
~~~~~~~~~~~~~~~~~~

Here is the output of its execution:

.. code:: sh

    Use multiplexed counters on the following program: 'test-program-decimal-sum'
    [i] CPU: -1
    [i] PID: 27434
    [i] GRP: -1
    [i] FLAGS :0
    [i] Child process
    Calculate the sum from 1 to 1000000000
    The sum from 1 to 1000000000 is 500000000500000000
    [i] Program executed with success!
    [i] Program execution statistics:
        2294909255 instructions (1170465393, 3038648675, 0.385193, 5957820416)
        2644229764 CPU cycles (1173441969, 3041631308, 0.385794, 6854001152)
        3175811531 ref CPU cycles (1409345911, 3044053068, 0.462983, 6859450880)
        181332 cache references (1409215142, 3044043432, 0.462942, 391695)
        19255 cache misses (1409241579, 3044032668, 0.462952, 41591)
        465894002 branch instructions (1409373243, 3044021435, 0.462997, 1006256832)
        37804 branch misses (1170082268, 3044008324, 0.384389, 98348)
        1177987814 salled cycles frontend (934077732, 3043895785, 0.306869, 3838729984)
        261422881 stalled cycles backend (934173926, 3023563142, 0.308965, 846125696)
        73105 cache L1D read misses (934143940, 3026550728, 0.308650, 236854)
        306056 cache L1I read misses (934094033, 3029529345, 0.308330, 992625)
        1222052530 cache L1D read accesses (934232040, 3032516294, 0.308072, 3966781184)
        2521463 cache L1I read accesses (934015063, 3035503344, 0.307697, 8194632)

The multiplexing seems to work correctly but if you look at the value, it tells us that the number of instructions is only 2 294 909 255 instead of 6 004 579 795. This difference is normal if we look at the other given values: 1 170 465 393 is the running time of the counter and 3 038 648 675 is the enabled time. The ratio between the running and the enabled time is:

.. math::

  ratio = \frac{running time}{enabled time} = \frac{1 170 465 393}{3 038 648 675} = 0.3852..

Let's estimate a value with this ratio: 

.. math::

  estimation = \frac{value}{ratio} = \frac{2 294 909 255}{0.3852..} = 5 957 820 416

Here is the error between the real value and the estimation:

.. math::

  error = \left(1 - \frac{estimation}{real value}\right) \times 100 = \left(1 - \frac{5 957 820 416}{6 004 579 795}\right) \times 100 = 0.7787\%

The error is **less than 1\%** which is a pretty good news!

not-multiplex-counters
~~~~~~~~~~~~~~~~~~~~~~

In this program, the variable ``pinned`` is set to ``1`` so the counters will not use multiplexing. 

Here is the output of its execution:

.. code:: sh

    Use multiplexed counters on the following program: 'test-program-decimal-sum'
    [i] CPU: -1
    [i] PID: 625
    [i] GRP: -1
    [i] FLAGS :0
    [i] Child process
    Calculate the sum from 1 to 1000000000
    The sum from 1 to 1000000000 is 500000000500000000
    [i] read_counter
    [i] read_counter
    [i] read_counter
    [i] read_counter
    [i] read_counter
    [i] read_counter
    [i] read_counter
    [e] Read failed: Success
        len=0 size=8 value=1001978507
    [i] read_counter
    [e] Read failed: Success
        len=0 size=8 value=1001978507
    [i] read_counter
    [e] Read failed: Success
        len=0 size=8 value=1001978507
    [i] read_counter
    [e] Read failed: Success
        len=0 size=8 value=1001978507
    [i] read_counter
    [e] Read failed: Success
        len=0 size=8 value=1001978507
    [i] read_counter
    [e] Read failed: Success
        len=0 size=8 value=1001978507
    [i] read_counter
    [e] Read failed: Success
        len=0 size=8 value=1001978507
    [i] Program executed with success!
    [i] Program execution statistics:
        6008950574 instructions
        6857751743 CPU cycles
        6936610997 ref CPU cycles
        464673 cache references
        43972 cache misses
        1001978507 branch instructions
        0 branch misses
        0 salled cycles frontend
        0 stalled cycles backend
        0 cache L1D read misses
        0 cache L1I read misses
        0 cache L1D read accesses
        0 cache L1I read accesses

As you can see, 6 over 13 counters responded correctly and the rest of them failed when I tried to read the value. Why 6 and not 7 counters as I have 7 hardware counters in my CPU? It is because some counters, as for example the CPU cycles counter, are associated to specific hardware counters and I suppose *perf_event* to not assign the counters accordingly to this constraint. 

Notice the number of instructions which is very close to the one obtained with ``instructions-counter``. 

multiplex-counters-whole-system
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you start this one without root permissions, you will get the error:

.. code:: sh

    [e] Attaching counter failed: Permission denied

The reason is on the ``syscall`` call. The ``pid`` value is ``-1`` to specify that we want to mesure the performance on the whole system and it requires the root permissions. Use ``sudo`` to run this program. 

Here is the output of its execution:

.. code:: sh

    Use multiplexed counters on the whole system for 2 seconds
    [i] PID: -1
    [i] GRP: -1
    [i] FLAGS :0
    [i] Number of CPUs: 4
    [i] Sleeping for 2 seconds...
    [i] Counters value:
        CPU 0
            287235994 instructions (2000745702, 2000747900, 0.999999, 287236320)
            413355395 CPU cycles (2000739159, 2000740736, 0.999999, 413355712)
            730735 cache misses (2000734544, 2000735415, 1.000000, 730735)
            2582648 branch misses (2000729869, 2000729869, 1.000000, 2582648)
        CPU 1
            82684286 instructions (2000716806, 2000720127, 0.999998, 82684424)
            139552520 CPU cycles (2000707867, 2000710289, 0.999999, 139552672)
            247311 cache misses (2000698675, 2000700025, 0.999999, 247311)
            640761 branch misses (2000693674, 2000693674, 1.000000, 640761)
        CPU 2
            236264030 instructions (2000680127, 2000682864, 0.999999, 236264368)
            385556585 CPU cycles (2000676740, 2000678771, 0.999999, 385556960)
            903232 cache misses (2000673353, 2000674464, 0.999999, 903232)
            2122355 branch misses (2000669260, 2000669260, 1.000000, 2122355)
        CPU 3
            6620760122 instructions (2000654624, 2000656959, 0.999999, 6620767744)
            4522373504 CPU cycles (2000650815, 2000652599, 0.999999, 4522377728)
            43612 cache misses (2000647334, 2000648224, 1.000000, 43612)
            6499901 branch misses (2000642556, 2000642556, 1.000000, 6499901)

The main difference is that we get value from counters by CPU. Indeed, we cannot give to ``syscall`` the parameter ``pid=-1`` and ``cpu=-1`` (it returns an error saying the configuration is invalid). My solution is to iterate the initialisation of the counters for each CPU (``cpu=0``, ``cpu=1`` and so on). 