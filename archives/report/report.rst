==========================
Internship in the ALF team
==========================

-----
Notes
-----

:Author: Guyon David
:Date: May - July 2014

Introduction
------------

First of all, you need to get the sources of the *hwloc* package: `hwloc-1.9.tar.gz <http://www.open-mpi.org/software/hwloc/v1.9/downloads/hwloc-1.9.tar.gz>`_. You will maybe be interested to look at this complete presentation/documentation of *hwloc*: `ComPAS tutorial <http://www.open-mpi.org/projects/hwloc/tutorials/20130115-ComPAS-hwloc-tutorial.pdf>`_. 

Here is a list of some basic commands:

* ``lstopo`` displays a graphic of your system architecture 
* ``hwloc-ls -v --of xml file.xml`` exports an XML version of the graphic in the specified file (``hwloc-ls file.xml`` for short)
* ``lstopo --no-io --of txt`` displays a neat ASCII version of your CPU architecture on the terminal

Before editing the source code and how to make a quick test
-----------------------------------------------------------

Before editing anything, execute ``./configure``, do the ``make`` command and finally ``make install`` if you want to install it (not required). For every modification of the sources, you will need to compile it again with ``make``. Then you can test your modifications with ``./utils/lstopo``.

The easiest way is to execute the following command: 

    ``make;echo;./utils/lstopo --no-io --no-legend``

Change the cache box background color
-------------------------------------

I found the method which constructs the window, *output_x11*. This method is called in *utils/lstopo.c* (line 648) and defined in the file *utils/lstopo-cairo.c* (line 250). 

The method call (*utils/lstopo.c*):

.. code:: C

    #ifdef LSTOPO_HAVE_GRAPHICS
    #if CAIRO_HAS_XLIB_SURFACE && defined HWLOC_HAVE_X11_KEYSYM
      printf("output_x11\n");
      if (getenv("DISPLAY")) {
        if (logical == -1)
          logical = 0;
        output_x11(topology, NULL, overwrite, logical, legend, verbose_mode);
      } else
    #endif /* CAIRO_HAS_XLIB_SURFACE */
    #ifdef HWLOC_WIN_SYS
      {
        printf("output_windows\n");
        if (logical == -1)
          logical = 0;
        output_windows(topology, NULL, overwrite, logical, legend, verbose_mode);
      }
    #endif
    #endif /* !LSTOPO_HAVE_GRAPHICS */

I will not paste the method implementation (123 lines) but here is its signature:

.. code:: C

    void output_x11(
      hwloc_topology_t topology, 
      const char *filename __hwloc_attribute_unused, 
      int overwrite __hwloc_attribute_unused, 
      int logical, 
      int legend, 
      int verbose_mode __hwloc_attribute_unused)

To draw the graphic, *output_x11* calls the method *topo_cairo_paint* which is implemented in the same file (line 96). Here is its implementation: 

.. code:: C

    static void topo_cairo_paint(
      struct draw_methods *methods, 
      int logical, 
      int legend, 
      hwloc_topology_t topology, 
      cairo_surface_t *cs)
    {
      cairo_t *c;
      c = cairo_create(cs);
      output_draw(methods, logical, legend, topology, c);
      cairo_show_page(c);
      cairo_destroy(c);
    }

As you can see, it is using the vector graphics library `Cairo <http://www.cairographics.org>`_. A drawing surface is instantiated and then the drawing is made by the method *output_draw* (line 1283 in */utils/lstopo-draw.c*) which then calls the method *fig* (line 1119 in the same file). The method *fig* calls the method *system_draw* to do the drawing of the boxes and then it displays the labels (hostname, index type and date). 

I found where and how are defined the drawing methods. They are in a static structure called *draw_methods*. We can see this structure when the method *output_draw* is called (it is the *methods* argument). Here is its definition: 

.. code:: C

    static struct draw_methods x11_draw_methods = {
      x11_start,
      null_declare_color,
      topo_cairo_box,
      topo_cairo_line,
      topo_cairo_text,
    };

The three methods *topo_cairo_box*, *topo_cairo_line* and *topo_cairo_text* are defined at the beginning of the file */utils/lstopo-cairo.c*. Their aim is to draw a box, a line or a text at a specific position and with a specific color using methods from the Cairo library. 

In the application, there are higher level drawing methods to directly draw the system, a socket, a cache, etc. Here is the *get_type_fun* method which gives the method to call depending on the object type (line 1199 in */utils/lstopo-draw.c*): 

.. code:: C

    static foo_draw get_type_fun(hwloc_obj_type_t type)
    {
      switch (type) {
        case HWLOC_OBJ_SYSTEM: return system_draw;
        case HWLOC_OBJ_MACHINE: return machine_draw;
        case HWLOC_OBJ_NODE: return node_draw;
        case HWLOC_OBJ_SOCKET: return socket_draw;
        case HWLOC_OBJ_CACHE: return cache_draw;
        case HWLOC_OBJ_CORE: return core_draw;
        case HWLOC_OBJ_PU: return pu_draw;
        case HWLOC_OBJ_GROUP: return group_draw;
        case HWLOC_OBJ_PCI_DEVICE: return pci_device_draw;
        case HWLOC_OBJ_OS_DEVICE: return os_device_draw;
        case HWLOC_OBJ_BRIDGE: return bridge_draw;
        default:
        case HWLOC_OBJ_MISC: return misc_draw;
        case HWLOC_OBJ_TYPE_MAX: assert(0);
      }
    }

To give a try I decided to change the background color from white to red for each cache box. Thus I starting editing the method *cache_draw*. Here is the implementation I have made to show a random red intensity on each cache:

.. code:: C

    /*
     * Get a random red intensity
     */
    int redintensity = rand()%255;

    lstopo_set_object_color(methods, topology, level, 0, &style);
    style.bg.g -= redintensity;
    style.bg.b -= redintensity;
    printf("  RED   = %d\n", style.bg.r);
    printf("  GREEN = %d\n", style.bg.g);
    printf("  BLUE  = %d\n", style.bg.b);
    methods->box(output, style.bg.r, style.bg.g, style.bg.b, depth, x, totwidth, y, myheight - gridsize);

It gave me the following result: 

.. image:: assets/lstopo-v1.png
   :height: 100px
   :width: 200 px
   :scale: 50 %
   :alt: alternate text
   :align: right

To be able to refresh the screen easily, I defined the "r" and the "R" keys as the refresh buttons. This modification is located in */utils/lstopo-cairo.c* in the *output_x11* method.

Let's make it dynamic
---------------------

Because it is using *XEvent* in the loop, the method *XNextEvent* blocks the program until an event occurs. The idea would be to migrate this blocking code to the *select* method. Thus we could set a timeout to catch an event or the timeout if nothing happens for a certain amount of time. 

Here is my implementation (only the new source code): 

.. code:: C

    /* Frequency: 1 second */
    struct timeval tv_period;
    tv_period.tv_sec = 1;
    tv_period.tv_usec = 0;
    /* Timeout used by select() */
    struct timeval tv; 
    tv.tv_sec = tv_period.tv_sec;
    tv.tv_usec = tv_period.tv_usec;
    /* Maximum next date */
    struct timeval tv_next_date;
    gettimeofday(&tv_next_date, 0);
    timeradd(&tv_next_date, &tv_period, &tv_next_date);
    /* Input file descriptor */
    fd_set in_fd;
    /* Get the file descriptor of the link with X11 */
    int dpy_fd = ConnectionNumber(disp->dpy);

    while (!finish) {
      FD_ZERO(&in_fd);
      FD_SET(dpy_fd, &in_fd);

      if (select(dpy_fd+1, &in_fd, 0, 0, &tv)) {
        printf("Event Received!\n");

        XNextEvent(disp->dpy, &e);
        ...

        /* Set tv with the remaining time */
        gettimeofday(&tv, 0);
        timersub(&tv_next_date, &tv, &tv);
      } else {
        printf("Timer Fired!\n");

        topo_cairo_paint(&x11_draw_methods, logical, legend, topology, disp->cs);

        /* Initialize next timeout */
        tv.tv_sec = tv_period.tv_sec;
        tv.tv_usec = tv_period.tv_usec;
        gettimeofday(&tv_next_date, 0);
        timeradd(&tv_next_date, &tv_period, &tv_next_date);
      }

      /* Handle XEvents and flush the input */
      while(XPending(disp->dpy))
        XNextEvent(disp->dpy, &e);
    }

As you can see in this snippet of code, the loop is divided into three parts: handling an event or a timeout and then clearing the events queue. The timeout triggers every second but when an event occurs during this second, the timeout never triggers. I added a bit of mathematics to make it works. Everytime an event occurs, the next alarm is calculate again depending on the current time. This is why there are three *timeval*: ``tv_period`` for the period (fixed), ``tv_next_date`` for the next alarm date and ``tv`` which will be used by *select* containing the period (changing at each event). 
 
After that, I implemented a new option in ``lstopo``: ``-r --refresh``. I wrote the corresponding code in */utils/lstopo.c*. The default value for the refresh period is 1000ms and the minimum value is 50ms. Under this value, the window starts to display some white lines, acting like an old TV. 
I had some trouble with the shared variable *refresh*. I did not have access to this variable in other files and it was because I forgot to add the following line in the */utils/lstopo.h* file:

.. code:: C

    extern unsigned int gridsize, fontsize, refresh;

Then I had to change the *timeval* structure called ``tv_period`` to use the ``refresh`` variable. Here is the modification: 

.. code:: C

    /* Period defined by the user */
    struct timeval tv_period;
    tv_period.tv_sec = (int)refresh/1000;
    tv_period.tv_usec = refresh%1000 * 1000;


First start with performance counters
-------------------------------------

Tools
~~~~~

To busy the CPU, a good solution is to start a program on a specific CPU with this command ``taskset -c 0 ./syracuse & echo $!`` and it prints the PID on the console. Then, to catch the process, use ``fg``. When the process is running, you can get information from it with the ``tiptop`` command. Press *p* in the *tiptop* UI and write the name of the running process. 

There is a sample C program to get the number of instructions executed which you can run with ``./cnt <PID>`` or with ``sudo ./cnt``. The first command allows you to get the number of instructions executed for a specific process and the second one scans the whole computer to give you the general number of instructions executed. To avoid using *sudo*, you can write *0* in */proc/sys/kernel/perf_event_paranoid*.

To get statistics of the whole system, use the following command: ``perf stat -a``. You will get something like that:

.. code:: sh

    Performance counter stats for 'system wide':

        10350,029663      task-clock (msec)         #    4,002 CPUs utilized           [100,00%]
               3 449      context-switches          #    0,333 K/sec                   [100,00%]
                 408      cpu-migrations            #    0,039 K/sec                   [100,00%]
              30 810      page-faults               #    0,003 M/sec                  
         900 257 644      cycles                    #    0,087 GHz                     [83,30%]
       1 183 641 329      stalled-cycles-frontend   #  131,48% frontend cycles idle    [83,26%]
         791 414 405      stalled-cycles-backend    #   87,91% backend  cycles idle    [66,73%]
         566 293 039      instructions              #    0,63  insns per cycle        
                                                    #    2,09  stalled cycles per insn [83,40%]
         116 414 972      branches                  #   11,248 M/sec                   [83,41%]
           5 361 532      branch-misses             #    4,61% of all branches         [83,31%]

         2,586270959 seconds time elapsed

To get *unavailable* perf events: ``dmesg | grep event``. When I run this command, I get *perf_event_intel: CPUID marked event: 'bus cycles' unavailable* which explains the error I get when I try to attach the bus cycles counter: *Attaching counter (PERF_TYPE_HARDWARE) failed for 6*. 

Understanding hardware counters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As I just said before, I cannot get information from the bus cycles counter because it is not available on my *Intel Code i3 M 350*. Thus it is normal and understandable to get errors when I try to read this counter. However, during my tests I got errors I did not understand first that I can explain now. 

When I execute the command ``papi_component_avail``, I can see that I only have 7 hardware counters in my CPU. So, how could I read from 10 counters if I only have 7 of them? **Sampling/Multiplexing** is the solution! At first, I tried to create my own multiplexed counters with a timer and a lot of ``syscall``, ``read`` and ``close`` but I realised that sampling support is already part of the system. Everything I needed to use it was here : */usr/include/linux/perf_event.h*. 

Finally I use the implemented multiplexing given in *perf_event.h*. I will explain how I use it in the multiplexing counters section. 

A neat solution
~~~~~~~~~~~~~~~

I created a new C file to implement my code for the performance counters: */utils/performance-tools.c*. There is also a new header file */utils/performance-tools.h*. To compile these files with the ``make`` command, I changed the two last lines of this block in the *Makefile.am* file: 

.. code:: Makefile

    lstopo_no_graphics_SOURCES = \
      lstopo.h \
      lstopo.c \
      lstopo-color.c \
      lstopo-draw.c \
      lstopo-fig.c \
      lstopo-text.c \
      lstopo-xml.c \
      performance-tools.h\
      performance-tools.c

Hardware counters with multiplexing
-----------------------------------

I made a test program and 4 programs using counters to get used to the multiplexing. They are located in *workaround-multiplex-counters/*:

 * ``test-program-decimal-sum`` is my test program on which I make my performance mesures
 * ``instructions-counter`` counts the number of instructions of the test program
 * ``multiplex-counters`` reads 13 counters with multiplexing
 * ``not-multiplex-counters`` does the same with no multiplexing
 * ``multiplex-counters-whole-system`` reads 4 counters by CPU during 2 seconds on the whole system

instructions-counter
~~~~~~~~~~~~~~~~~~~~

Here is the output of its execution:

.. code:: sh

    Calculate the number of instructions of the program 'test-program-decimal-sum'
    [i] CPU: -1
    [i] PID: 21494
    [i] GRP: -1
    [i] FLAGS :0
    [i] Child process
    Calculate the sum from 1 to 1000000000
    The sum from 1 to 1000000000 is 500000000500000000
    [i] Program executed with success!
    [i] The program executed in 6004579795 instructions

As you can see, it tells us that the program needed 6 004 579 795 instructions to execute. 

multiplex-counters
~~~~~~~~~~~~~~~~~~

Here is the output of its execution:

.. code:: sh

    Use multiplexed counters on the following program: 'test-program-decimal-sum'
    [i] CPU: -1
    [i] PID: 27434
    [i] GRP: -1
    [i] FLAGS :0
    [i] Child process
    Calculate the sum from 1 to 1000000000
    The sum from 1 to 1000000000 is 500000000500000000
    [i] Program executed with success!
    [i] Program execution statistics:
        2294909255 instructions (1170465393, 3038648675, 0.385193, 5957820416)
        2644229764 CPU cycles (1173441969, 3041631308, 0.385794, 6854001152)
        3175811531 ref CPU cycles (1409345911, 3044053068, 0.462983, 6859450880)
        181332 cache references (1409215142, 3044043432, 0.462942, 391695)
        19255 cache misses (1409241579, 3044032668, 0.462952, 41591)
        465894002 branch instructions (1409373243, 3044021435, 0.462997, 1006256832)
        37804 branch misses (1170082268, 3044008324, 0.384389, 98348)
        1177987814 salled cycles frontend (934077732, 3043895785, 0.306869, 3838729984)
        261422881 stalled cycles backend (934173926, 3023563142, 0.308965, 846125696)
        73105 cache L1D read misses (934143940, 3026550728, 0.308650, 236854)
        306056 cache L1I read misses (934094033, 3029529345, 0.308330, 992625)
        1222052530 cache L1D read accesses (934232040, 3032516294, 0.308072, 3966781184)
        2521463 cache L1I read accesses (934015063, 3035503344, 0.307697, 8194632)

The multiplexing seems to work correctly but if you look at the value, it tells us that the number of instructions is only 2 294 909 255 instead of 6 004 579 795. This difference is normal if we look at the other given values: 1 170 465 393 is the running time of the counter and 3 038 648 675 is the enabled time. The ratio between the running and the enabled time is:

.. math::

  ratio = \frac{running time}{enabled time} = \frac{1 170 465 393}{3 038 648 675} = 0.3852..

Let's estimate a value with this ratio: 

.. math::

  estimation = \frac{value}{ratio} = \frac{2 294 909 255}{0.3852..} = 5 957 820 416

Here is the error between the real value and the estimation:

.. math::

  error = \left(1 - \frac{estimation}{real value}\right) \times 100 = \left(1 - \frac{5 957 820 416}{6 004 579 795}\right) \times 100 = 0.7787\%

The error is **less than 1\%** which is a pretty good news!

not-multiplex-counters
~~~~~~~~~~~~~~~~~~~~~~

In this program, the variable ``pinned`` is set to ``1`` so the counters will not use multiplexing. 

Here is the output of its execution:

.. code:: sh

    Use multiplexed counters on the following program: 'test-program-decimal-sum'
    [i] CPU: -1
    [i] PID: 625
    [i] GRP: -1
    [i] FLAGS :0
    [i] Child process
    Calculate the sum from 1 to 1000000000
    The sum from 1 to 1000000000 is 500000000500000000
    [i] read_counter
    [i] read_counter
    [i] read_counter
    [i] read_counter
    [i] read_counter
    [i] read_counter
    [i] read_counter
    [e] Read failed: Success
        len=0 size=8 value=1001978507
    [i] read_counter
    [e] Read failed: Success
        len=0 size=8 value=1001978507
    [i] read_counter
    [e] Read failed: Success
        len=0 size=8 value=1001978507
    [i] read_counter
    [e] Read failed: Success
        len=0 size=8 value=1001978507
    [i] read_counter
    [e] Read failed: Success
        len=0 size=8 value=1001978507
    [i] read_counter
    [e] Read failed: Success
        len=0 size=8 value=1001978507
    [i] read_counter
    [e] Read failed: Success
        len=0 size=8 value=1001978507
    [i] Program executed with success!
    [i] Program execution statistics:
        6008950574 instructions
        6857751743 CPU cycles
        6936610997 ref CPU cycles
        464673 cache references
        43972 cache misses
        1001978507 branch instructions
        0 branch misses
        0 salled cycles frontend
        0 stalled cycles backend
        0 cache L1D read misses
        0 cache L1I read misses
        0 cache L1D read accesses
        0 cache L1I read accesses

As you can see, 6 over 13 counters responded correctly and the rest of them failed when I tried to read the value. Why 6 and not 7 counters as I have 7 hardware counters in my CPU? It is because some counters, as for example the CPU cycles counter, are associated to specific hardware counters and I suppose *perf_event* to not assign the counters accordingly to this constraint. 

Notice the number of instructions which is very close to the one obtained with ``instructions-counter``. 

multiplex-counters-whole-system
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you start this one without root permissions, you will get the error:

.. code:: sh

    [e] Attaching counter failed: Permission denied

The reason is on the ``syscall`` call. The ``pid`` value is ``-1`` to specify that we want to mesure the performance on the whole system and it requires the root permissions. Use ``sudo`` to run this program. 

Here is the output of its execution:

.. code:: sh

    Use multiplexed counters on the whole system for 2 seconds
    [i] PID: -1
    [i] GRP: -1
    [i] FLAGS :0
    [i] Number of CPUs: 4
    [i] Sleeping for 2 seconds...
    [i] Counters value:
        CPU 0
            287235994 instructions (2000745702, 2000747900, 0.999999, 287236320)
            413355395 CPU cycles (2000739159, 2000740736, 0.999999, 413355712)
            730735 cache misses (2000734544, 2000735415, 1.000000, 730735)
            2582648 branch misses (2000729869, 2000729869, 1.000000, 2582648)
        CPU 1
            82684286 instructions (2000716806, 2000720127, 0.999998, 82684424)
            139552520 CPU cycles (2000707867, 2000710289, 0.999999, 139552672)
            247311 cache misses (2000698675, 2000700025, 0.999999, 247311)
            640761 branch misses (2000693674, 2000693674, 1.000000, 640761)
        CPU 2
            236264030 instructions (2000680127, 2000682864, 0.999999, 236264368)
            385556585 CPU cycles (2000676740, 2000678771, 0.999999, 385556960)
            903232 cache misses (2000673353, 2000674464, 0.999999, 903232)
            2122355 branch misses (2000669260, 2000669260, 1.000000, 2122355)
        CPU 3
            6620760122 instructions (2000654624, 2000656959, 0.999999, 6620767744)
            4522373504 CPU cycles (2000650815, 2000652599, 0.999999, 4522377728)
            43612 cache misses (2000647334, 2000648224, 1.000000, 43612)
            6499901 branch misses (2000642556, 2000642556, 1.000000, 6499901)

The main difference is that we get value from counters by CPU. Indeed, we cannot give to ``syscall`` the parameter ``pid=-1`` and ``cpu=-1`` (it returns an error saying the configuration is invalid). My solution is to iterate the initialisation of the counters for each CPU (``cpu=0``, ``cpu=1`` and so on). 

Understanding types of hardware counters
----------------------------------------

Instructions
~~~~~~~~~~~~

CPU cycles
~~~~~~~~~~

Cache misses
~~~~~~~~~~~~

And so on
~~~~~~~~~

Integration of multiplex counters on lstopo
-------------------------------------------
