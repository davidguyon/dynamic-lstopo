#include <time.h>
#include <stdio.h>
#include <stdlib.h>

// L1
//#define N 8*1024
// 2*L1
// #define N 16*1024
// L3
// #define N 750*1024
// 2*L3
#define N 2*750*1024
// 256M of RAM
//#define N 64*1024*1024
// 512M of RAM
//#define N 128*1024*1024
// 1024M of RAM
//#define N 256*1024*1024
// 2048M of RAM
//#define N 512*1024*1024

int main(int argc, char* argv[])
{
  printf("Cache brute test\n");
  int *arr;
  arr = (int*) malloc(sizeof(int) * N);

  srandom((unsigned int)time(NULL));

  int nb;
  for(nb = 0 ; nb < N ; nb++)
    arr[nb] = random() % 1024*1024;

  for(nb = 0 ; nb < N ; nb++)
    printf("%d\n", arr[nb]);

  while(1) {
    long int i = random() % N;
    long int j = random() % N;
    long int k = random() % N;
    //printf("%ld %ld %ld\n", i, j, k);
    // printf("%d %d\n", arr[j], arr[k]);
    arr[i] = arr[j] - arr[k];
    arr[j] += 42;
    arr[k] -= 42;
    printf("%d\n", arr[i]);
  }
  return 1;
}
