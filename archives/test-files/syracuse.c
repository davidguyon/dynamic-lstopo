#include <stdio.h>
#include <stdlib.h>
#ifdef USE_SIGS
#include <signal.h>
#endif
#include <unistd.h>

unsigned long long n = 3;

#ifdef USE_SIGS
void dump(int sig)
{
  printf("%lld\n", n);
  alarm(1);
}
#endif

int syracuse(int n)
{
  int threshold = n-1;

  while (n > threshold) {
    if (n & 1)
      n = 3*n+1;
    else
      n >>= 1;
  }
  return 0;
}


int main(int argc, char* argv[])
{
  if (argc > 1) {
    n = atoi(argv[1]);
    n = (n / 4)*4-1;
  }

  printf("Initial value: %d\n", n);

#ifdef USE_SIGS
  signal(SIGALRM, dump);
  alarm(1);
#endif

  while (1) {
    syracuse(n);
    n+=4;
  }

}
