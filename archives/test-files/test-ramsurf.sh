#!/bin/bash
echo "taskset -c 0 ./ramsurf1.5-bad & taskset -c 1 ./ramsurf1.5-good"
echo "Press 'q' to leave the test"
taskset -c 2 ./ramsurf1.5-bad &
foo=true
while $foo
do
  taskset -c 3 ./ramsurf1.5-good
  read -t 1 -n 1 -s key 
  if [[ $key = "q" ]]; then foo=false; fi
done
killall ramsurf1.5-bad
