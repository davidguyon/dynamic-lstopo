#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>
#include <linux/perf_event.h>

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#define RESET "\033[0m"

#if defined(__x86_64__)
  #define __NR_perf_counter_open  298
#elif defined(__i386__)
  #define __NR_perf_counter_open  336
#else
  #error What syscall???
#endif

#define PGR "test-program-decimal-sum"

/* Constants */
const int cpu = -1;
const int grp = -1;
const int flags = 0;

void main(int argc, char* argv[])
{
  printf("Calculate the number of instruction of the program '%s'\n", PGR);

  int fd;
  int len;
  uint64_t value;

  struct perf_event_attr ev = {0};
  ev.type = PERF_TYPE_HARDWARE;
  ev.size = sizeof(ev);
  ev.config = PERF_COUNT_HW_INSTRUCTIONS;
  ev.read_format = 0;
  ev.disabled = 1;
  ev.inherit = 1;
  ev.enable_on_exec = 1;

  pid_t pid = fork();

  if (pid == -1) {
    perror("[e] Fork failed");
    exit(EXIT_FAILURE);
  }
  else if (pid == 0) {
    printf("[i] Child process\n");
    if(execl(PGR, "./" PGR, 0, 0) == -1) {
      perror("[e] Execl failed");
    }
    _exit(EXIT_SUCCESS);  /* exit() is unreliable here, so _exit must be used */
  }
  else {
    printf("[i] CPU: %d\n", cpu);
    printf("[i] PID: %d\n", pid);
    printf("[i] GRP: %d\n", grp);
    printf("[i] FLAGS :%d\n", flags);
    fd = syscall(__NR_perf_counter_open, &ev, pid, cpu, grp, flags);
    if (fd < 0 && fd > -4096) {
      fd = -1;
      perror("[e] Attaching instructions counter failed");
    }
    int status;
    (void)waitpid(pid, &status, 0);

    len = read(fd, &value, sizeof(value));
    if (len != sizeof(value)) {
      perror("[e] Read failed");
      printf("[e] Could not read instructions counter\n");
      return;
    }

    if(status == -1)
      printf("[e] An error occured with waitpid(3)\n");
    else
      printf("[i] Program executed with success!\n");

    printf("[i] The program executed in " KGRN "%" PRIu64 RESET " instructions\n", value);
  }
}
