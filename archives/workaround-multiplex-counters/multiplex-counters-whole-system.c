#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>
#include <linux/perf_event.h>

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#define RESET "\033[0m"

#if defined(__x86_64__)
  #define __NR_perf_counter_open  298
#elif defined(__i386__)
  #define __NR_perf_counter_open  336
#else
  #error What syscall???
#endif

/* Constants */
const int pid = -1;
const int grp = -1;
const int flags = 0;

struct read_format {
  uint64_t value;         /* The value of the event */
  uint64_t time_enabled;  /* if PERF_FORMAT_TOTAL_TIME_ENABLED */
  uint64_t time_running;  /* if PERF_FORMAT_TOTAL_TIME_RUNNING */
};

int init_counter(int cpu, int type, int conf)
{
  struct perf_event_attr ev = {0};
  ev.type = type;
  ev.size = sizeof(ev);
  ev.config = conf;
  ev.read_format = PERF_FORMAT_TOTAL_TIME_ENABLED
                 | PERF_FORMAT_TOTAL_TIME_RUNNING;
  ev.disabled = 0;
  ev.inherit = 1;
  ev.enable_on_exec = 0;

  int fd = syscall(__NR_perf_counter_open, &ev, pid, cpu, grp, flags);
  if (fd < 0 && fd > -4096) {
    fd = -1;
    perror("[e] Attaching counter failed");
  }

  return fd;
}

struct read_format read_counter(int fd)
{
  struct read_format value;
  int len = read(fd, &value, sizeof(value));
  if (len != sizeof(value)) {
    perror("[e] Read failed");
    exit(EXIT_FAILURE);
  }

  return value;
}

void print_counter(struct read_format counter_value, char * name)
{
  float ratio = (float) counter_value.time_running / (float) counter_value.time_enabled;
  uint64_t estimated = counter_value.value / ratio;
  printf(KGRN "\t\t%" PRIu64 RESET " %s (" KCYN "%" PRIu64 RESET ", " KCYN "%" PRIu64 RESET ", " KCYN "%.6f" RESET ", " KCYN "%" PRIu64 RESET ")\n",
    counter_value.value, name, counter_value.time_running, counter_value.time_enabled, ratio, estimated);
}

void main(int argc, char* argv[])
{
  printf("Use multiplexed counters on the whole system for 2 seconds\n");

  int nb_cpu = sysconf(_SC_NPROCESSORS_ONLN);

  printf("[i] PID: %d\n", pid);
  printf("[i] GRP: %d\n", grp);
  printf("[i] FLAGS :%d\n", flags);
  printf("[i] Number of CPUs: %d\n", nb_cpu);

  /* Initialize all counters */
  int fd_cpu[nb_cpu][4];
  struct read_format res_cpu[nb_cpu][4];
  int i = 0;
  for(i=0 ; i<nb_cpu ; i++)
  {
    fd_cpu[i][0] = init_counter(i, PERF_TYPE_HARDWARE, PERF_COUNT_HW_INSTRUCTIONS);
    fd_cpu[i][1] = init_counter(i, PERF_TYPE_HARDWARE, PERF_COUNT_HW_CPU_CYCLES);
    fd_cpu[i][2] = init_counter(i, PERF_TYPE_HARDWARE, PERF_COUNT_HW_CACHE_MISSES);
    fd_cpu[i][3] = init_counter(i, PERF_TYPE_HARDWARE, PERF_COUNT_HW_BRANCH_MISSES);
  }

  printf("[i] Sleeping for 2 seconds...\n");
  sleep(2); // Sleep for 2 seconds

  /* Read the counters value */
  for(i=0 ; i<nb_cpu ; i++)
  {
    res_cpu[i][0] = read_counter(fd_cpu[i][0]);
    res_cpu[i][1] = read_counter(fd_cpu[i][1]);
    res_cpu[i][2] = read_counter(fd_cpu[i][2]);
    res_cpu[i][3] = read_counter(fd_cpu[i][3]);
  }

  printf("[i] Counters value:\n");
  for(i=0 ; i<nb_cpu ; i++)
  {
    printf("\tCPU %d\n", i);
    print_counter(res_cpu[i][0], "instructions");
    print_counter(res_cpu[i][1], "CPU cycles");
    print_counter(res_cpu[i][2], "cache misses");
    print_counter(res_cpu[i][3], "branch misses");
  }
}