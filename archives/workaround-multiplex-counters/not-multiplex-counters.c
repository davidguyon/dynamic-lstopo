#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>
#include <linux/perf_event.h>

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#define RESET "\033[0m"

#if defined(__x86_64__)
  #define __NR_perf_counter_open  298
#elif defined(__i386__)
  #define __NR_perf_counter_open  336
#else
  #error What syscall???
#endif

#define PGR "test-program-decimal-sum"

/* Constants */
const int cpu = -1;
const int grp = -1;
const int flags = 0;

int init_counter(pid_t pid, int type, int conf)
{
  struct perf_event_attr ev = {0};
  ev.type = type;
  ev.size = sizeof(ev);
  ev.config = conf;
  // ev.read_format = PERF_FORMAT_TOTAL_TIME_ENABLED
  //                | PERF_FORMAT_TOTAL_TIME_RUNNING;
  // ev.disabled = 0;
  // ev.inherit = 1;
  // ev.enable_on_exec = 0;
  ev.pinned = 1;
  // ev.exclude_kernel = 1;
  // ev.exclude_hv = 1;

  int fd = syscall(__NR_perf_counter_open, &ev, pid, cpu, grp, flags);
  if (fd < 0 && fd > -4096) {
    fd = -1;
    perror("[e] Attaching counter failed");
  }

  return fd;
}

// struct read_format read_counter(int fd)
uint64_t read_counter(int fd)
{
  printf("[i] read_counter\n");
  uint64_t value;
  int len = read(fd, &value, sizeof(value));
  if (len != sizeof(value)) {
    perror("[e] Read failed");
    printf("    len=%d size=%d value=%" PRIu64 "\n", len, sizeof(value), value);
    return 0;
  }

  return value;
}

// void print_counter(struct read_format counter_value, char * name)
void print_counter(uint64_t counter_value, char * name)
{
  printf(KGRN "\t%" PRIu64 RESET " %s\n", counter_value, name);
}

void main(int argc, char* argv[])
{
  printf("Use multiplexed counters on the following program: '%s'\n", PGR);

  pid_t pid = fork();

  /*
   * Fork failed
   */
  if (pid == -1) {
    perror("[e] Fork failed");
    exit(EXIT_FAILURE);
  }
  /*
   * Child process
   */
  else if (pid == 0) {
    printf("[i] Child process\n");
    if(execl(PGR, "./" PGR, 0, 0) == -1) {
      perror("[e] Execl failed");
    }
    _exit(EXIT_SUCCESS);
  }
  /*
   * Parent process
   */
  else {
    printf("[i] CPU: %d\n", cpu);
    printf("[i] PID: %d\n", pid);
    printf("[i] GRP: %d\n", grp);
    printf("[i] FLAGS :%d\n", flags);

    /* Initialize all counters */
    int fd_instructions = 
      init_counter(pid, PERF_TYPE_HARDWARE, PERF_COUNT_HW_INSTRUCTIONS);
    int fd_cpu_cycles = 
      init_counter(pid, PERF_TYPE_HARDWARE, PERF_COUNT_HW_CPU_CYCLES);
    int fd_ref_cpu_cycles = 
      init_counter(pid, PERF_TYPE_HARDWARE, PERF_COUNT_HW_REF_CPU_CYCLES);
    int fd_cache_ref =
      init_counter(pid, PERF_TYPE_HARDWARE, PERF_COUNT_HW_CACHE_REFERENCES);
    int fd_cache_misses = 
      init_counter(pid, PERF_TYPE_HARDWARE, PERF_COUNT_HW_CACHE_MISSES);
    int fd_branch_instructions =
      init_counter(pid, PERF_TYPE_HARDWARE, PERF_COUNT_HW_BRANCH_INSTRUCTIONS);
    int fd_branch_misses =
      init_counter(pid, PERF_TYPE_HARDWARE, PERF_COUNT_HW_BRANCH_MISSES);
    int fd_stalled_frontend =
      init_counter(pid, PERF_TYPE_HARDWARE, PERF_COUNT_HW_STALLED_CYCLES_FRONTEND);
    int fd_stalled_backend =
      init_counter(pid, PERF_TYPE_HARDWARE, PERF_COUNT_HW_STALLED_CYCLES_BACKEND);
    int fd_cache_L1D_read_miss =
      init_counter(pid, PERF_TYPE_HW_CACHE, PERF_COUNT_HW_CACHE_L1D
                                          | PERF_COUNT_HW_CACHE_OP_READ << 8
                                          | PERF_COUNT_HW_CACHE_RESULT_MISS << 16);
    int fd_cache_L1I_read_miss =
      init_counter(pid, PERF_TYPE_HW_CACHE, PERF_COUNT_HW_CACHE_L1I
                                          | PERF_COUNT_HW_CACHE_OP_READ << 8
                                          | PERF_COUNT_HW_CACHE_RESULT_MISS << 16);;
    int fd_cache_L1D_read =
      init_counter(pid, PERF_TYPE_HW_CACHE, PERF_COUNT_HW_CACHE_L1D
                                          | PERF_COUNT_HW_CACHE_OP_READ << 8
                                          | PERF_COUNT_HW_CACHE_RESULT_ACCESS << 16);
    int fd_cache_L1I_read =
      init_counter(pid, PERF_TYPE_HW_CACHE, PERF_COUNT_HW_CACHE_L1I
                                          | PERF_COUNT_HW_CACHE_OP_READ << 8
                                          | PERF_COUNT_HW_CACHE_RESULT_ACCESS << 16);;

    /* Wait for the child process to terminate */
    int status;
    (void)waitpid(pid, &status, 0);

    /* Read the counters value */
    uint64_t nb_instructions        = read_counter(fd_instructions);
    uint64_t nb_cpu_cycles          = read_counter(fd_cpu_cycles);
    uint64_t nb_ref_cpu_cycles      = read_counter(fd_ref_cpu_cycles);
    uint64_t nb_cache_ref           = read_counter(fd_cache_ref);
    uint64_t nb_cache_misses        = read_counter(fd_cache_misses);
    uint64_t nb_branch_instructions = read_counter(fd_branch_instructions);
    uint64_t nb_branch_misses       = read_counter(fd_branch_misses);
    uint64_t nb_stalled_frontend    = read_counter(fd_stalled_frontend);
    uint64_t nb_stalled_backend     = read_counter(fd_stalled_backend);
    uint64_t nb_cache_L1D_read_miss = read_counter(fd_cache_L1D_read_miss);
    uint64_t nb_cache_L1I_read_miss = read_counter(fd_cache_L1I_read_miss);
    uint64_t nb_cache_L1D_read      = read_counter(fd_cache_L1D_read);
    uint64_t nb_cache_L1I_read      = read_counter(fd_cache_L1I_read);

    if(status == -1)
      printf("[e] An error occured with waitpid(3)\n");
    else
      printf("[i] Program executed with success!\n");

    printf("[i] Program execution statistics:\n");
    print_counter(nb_instructions, "instructions");
    print_counter(nb_cpu_cycles, "CPU cycles");
    print_counter(nb_ref_cpu_cycles, "ref CPU cycles");
    print_counter(nb_cache_ref, "cache references");
    print_counter(nb_cache_misses, "cache misses");
    print_counter(nb_branch_instructions, "branch instructions");
    print_counter(nb_branch_misses, "branch misses");
    print_counter(nb_stalled_frontend, "salled cycles frontend");
    print_counter(nb_stalled_backend, "stalled cycles backend");
    print_counter(nb_cache_L1D_read_miss, "cache L1D read misses");
    print_counter(nb_cache_L1I_read_miss, "cache L1I read misses");
    print_counter(nb_cache_L1D_read, "cache L1D read accesses");
    print_counter(nb_cache_L1I_read, "cache L1I read accesses");
  }
}