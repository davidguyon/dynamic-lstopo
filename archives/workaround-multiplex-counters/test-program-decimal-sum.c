#include <stdio.h>

#define MIN 1
#define MAX 1000000000

int main(int argc, char* argv[])
{
  int i = MIN;
  unsigned long sum = 0;

  printf("Calculate the sum from %d to %d\n", MIN, MAX);

  for(;i<=MAX;i++)
  {
    sum += i;
  }

  printf("The sum from %d to %d is %lu\n", MIN, MAX, sum);
}
