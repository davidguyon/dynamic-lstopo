#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <dirent.h>
#include <sys/sysinfo.h>

/*
 * keywords for colors in printf
 */
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#define RESET "\033[0m"

static void timer_handler(int sig);

int hertz;
char* pid;
char* path;
int nb_cpus;
int nb_tasks;
long last_uptime;

struct stat {
  int processor;
  long int cutime;
  long int cstime;
  long unsigned int utime;
  long unsigned int stime;
};

struct stat* last_stat;

/**
 * get_nb_of_tasks
 * return the number of file in the /proc/<pid>/task/ folder
 * 
 * @param:
 *    void
 *
 * @return:
 *    int
 */
int get_nb_of_tasks()
{
  DIR* dir;
  int nb = 0;
  char* path_task;
  struct dirent *entry;

  path_task = malloc(strlen(path) + strlen("/task/") +  1);
  strcpy(path_task, path);
  strcat(path_task, "/task/");

  dir = opendir (path_task);
  while ((entry = readdir (dir)) != NULL) {
    if( /* ignore . and .. */
      !strcmp(entry->d_name, ".") ||
      !strcmp(entry->d_name, "..")) {
      continue;
    }

    nb++;
  }
  closedir(dir);

  return nb;
}

/**
 * get_nb_of_cpus
 * return the number of available CPUs in the system
 *
 * @param:
 *    void
 *
 * @return:
 *    int
 */
int get_nb_of_cpus()
{
  int nb = sysconf(_SC_NPROCESSORS_ONLN);
  if (nb < 1) {
    fprintf(stderr, "[e] Could not determine number of CPUs\n");
    exit (EXIT_FAILURE);
  }

  return nb;
}

/**
 * get_delta_uptime_in_ms
 * return the number of milliseconds spent since the last time this method was
 * called. It uses the global variable `last_uptime`. 
 *
 * @param:
 *    void
 *
 * @return:
 *    long
 */
long get_delta_uptime_in_ms()
{
  long ms;
  time_t s;
  struct timespec spec;
  clock_gettime(CLOCK_REALTIME, &spec);

  s  = spec.tv_sec;
  ms = round(spec.tv_nsec / 1.0e6); // Convert nanoseconds to milliseconds
  ms += s * 1000; // Convert seconds to milliseconds and them to ms

  long delta = ms - last_uptime;
  last_uptime = ms;

  printf("[i] uptime: %ld\n", delta);

  return delta;
}

/**
 * open_file
 * open the file located at the path given in parameter and return its pointer
 *
 * @param:
 *    char* path
 *
 * @return:
 *    FILE*
 */
FILE* open_file(char* path)
{
  FILE* fp = fopen(path, "r");
  if(fp == NULL) {
    perror("[e] Fopen failed");
    fprintf(stderr, "[e] The given pid does not exist\n");
  }

  return fp;
}

/**
 * close_file
 * close the file given in parameter
 *
 * @param:
 *    FILE* fp
 *
 * @return:
 *    void
 */
void close_file(FILE* fp)
{
  if(fclose(fp) != 0)
    perror("[e] Fclose failed");
}

/**
 * read_stat_file
 * read the `stat` file of the <pid>/<tid> given in the `path` parameter and
 * fill the delta struct with the difference between the new values and the
 * last values (`last` parameter)
 *
 * @param:
 *    char* path
 *    struct stat* delta
 *    struct stat* last
 *
 * @return:
 *    void
 */
void read_stat_file(char* path, struct stat* delta, struct stat* last)
{
  FILE* fp;
  char* path_stat;
  struct stat new = {0};

  /* Initialize the path to the /stat file */
  path_stat = malloc(strlen(path) + strlen("/stat") +  1);
  strcpy(path_stat, path);
  strcat(path_stat, "/stat");

  /* Open the /stat file and read the values */
  fp = open_file(path_stat);
  fscanf(fp, 
    "%*d %*s %*c %*d %*d %*d %*d %*d %*u %*u %*u %*u %*u %lu %lu %ld %ld %*d %*d %*d %*d %*u %*u %*d %*u %*u %*u %*u %*u %*u %*u %*u %*u %*u %*u %*u %*u %*d %d", 
    &new.utime, 
    &new.stime, 
    &new.cutime, 
    &new.cstime,
    &new.processor
  );
  close_file(fp);

  /* Calculate the delta values */
  delta->utime  = new.utime  - last->utime;
  delta->stime  = new.stime  - last->stime;
  delta->cutime = new.cutime - last->cutime;
  delta->cstime = new.cstime - last->cstime;
  delta->processor = new.processor;

  /* Save the new values in the _last_ structure */
  last->utime = new.utime;
  last->stime = new.stime;
  last->cutime = new.cutime;
  last->cstime = new.cstime;
  last->processor = new.processor;
  printf("[i] %s\n\tutime %ld\n\tstime %ld\n\tcutime %lu\n\tcstime %lu\n\tprocessor %d\n", path, delta->utime, delta->stime, delta->cutime, delta->cstime, delta->processor);
}

/**
 * set_timer
 * initialize the value of the timer SIGALRM and define the callback method
 *
 * @param:
 *    void
 *
 * @return:
 *    void
 */
void set_timer()
{
  signal(SIGALRM, timer_handler);
  // ualarm(500000, 500000); /* after 100 ms, every 100 ms */
  alarm(1);
}

/**
 * timer_handler
 * callback method which is called by the internal timer every X milliseconds
 * it handles the calculation of the CPU usage by core for the given <pid>
 *
 * @param:
 *    int sig
 *
 * @return:
 *    static void
 */
static void timer_handler(int sig)
{
  int var; /* dummy variable for iterations */

  signal(SIGALRM, SIG_IGN); /* ignore this signal */
  printf("\n[i] Alarm triggered!\n");

  /* Get uptime */
  long delta_uptime = get_delta_uptime_in_ms();

  /* Initialize the delta struct (contains <pid> + all <tid>) */
  struct stat delta_stat[nb_tasks];

  /* Read all <tid>/stat */
  DIR* dir;
  char* path_task;
  struct dirent *entry;

  path_task = malloc(strlen(path) + strlen("/task/") +  1);
  strcpy(path_task, path);
  strcat(path_task, "/task/");

  dir = opendir (path_task);
  var = 0;
  while ((entry = readdir (dir)) != NULL) {
    if( /* ignore . and .. */
      !strcmp(entry->d_name, ".") ||
      !strcmp(entry->d_name, "..")) {
      continue;
    }

    /* Initialize /proc/<pid>/task/<tid> */
    char* path_task_tid;
    path_task_tid = malloc(strlen(path_task) + strlen(entry->d_name) + 1);
    strcpy(path_task_tid, path_task);
    strcat(path_task_tid, entry->d_name);

    printf("[i] Reading %s\n", path_task_tid);
    read_stat_file(path_task_tid, &delta_stat[var], &last_stat[var]);
    var++;
  }
  closedir(dir);

  /* Calculate process duration by CPU */
  long unsigned int process_duration[nb_cpus];
  memset(process_duration, 0, sizeof(process_duration));  
  var = 0;
  while(var < (nb_tasks)) {
    int cpu = delta_stat[var].processor;
    process_duration[cpu] += delta_stat[var].utime;
    process_duration[cpu] += delta_stat[var].stime;
    process_duration[cpu] += delta_stat[var].cutime;
    process_duration[cpu] += delta_stat[var].cstime;
    var++;
  }

  for(var = 0 ; var < nb_cpus ; var++)
    printf("[i] Process duration for CPU%d: %lu\n", var, process_duration[var]);

  /* Calculate CPU usage by CPU */
  float cpu_usage[nb_cpus];
  memset(cpu_usage, 0, sizeof(cpu_usage));
  var = 0;
  while(var < nb_cpus) {
    float a = (float)process_duration[var] / (float)hertz;
    cpu_usage[var] = 100 * ( a / ((float)delta_uptime/1000) );
    var++;
  }

  for(var = 0 ; var < nb_cpus ; var++)
    printf("[i] CPU%d usage: %3.1f%%\n", var, cpu_usage[var]);

  set_timer();
}

/**
 * main
 * entry point of the program
 * 
 * @param:
 *    int argc
 *    char* argv[]
 *
 * @return:
 *    int
 */
int main(int argc, char* argv[])
{
  printf("[i] Calculate the CPU usage for the process given in parameter\n");
  
  /* Check the pid parameter */
  if( (argc =! 2) ) {
    fprintf(stderr, "[e] Wrong parameters! The PID of a running process have to be set as parameter\n");
    return -1;
  } else {
    printf("[i] Process ID is %s\n", argv[1]);
    pid = argv[1];
  }

  /* Get path depending on the pid */
  path = malloc(strlen("/proc/") + strlen(argv[1]) +  1);
  strcpy(path, "/proc/");
  strcat(path, argv[1]);

  /* Get hertz */
  hertz = sysconf(_SC_CLK_TCK);
  printf("[i] hertz  = %d\n", hertz);

  nb_tasks = get_nb_of_tasks();
  last_stat = malloc(nb_tasks * sizeof(struct stat));

  /* FIX ME: _last_stat_ initialized at {0} */
  int var = 0;
  for(; var < nb_tasks ; var++) {
    last_stat[var].utime     = 0;
    last_stat[var].stime     = 0;
    last_stat[var].cutime    = 0;
    last_stat[var].cstime    = 0;
    last_stat[var].processor = 0;
  }

  nb_cpus = get_nb_of_cpus();

  set_timer();

  while(1);

  return 0;
}