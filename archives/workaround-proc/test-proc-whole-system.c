#include <time.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>

/*
 * keywords for colors in printf
 */
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#define RESET "\033[0m"

static void timer_handler(int sig);

int hertz;
char* path;
int nb_cpus;
long last_uptime;

struct cpu_stat {
  double _user;
  double _nice;
  double _system;
  double _idle;
};

struct cpu_stat* last_cpu_stat;

/**
 * get_nb_of_cpus
 * return the number of available CPUs in the system
 *
 * @param:
 *    void
 *
 * @return:
 *    int
 */
int get_nb_of_cpus()
{
  int nb = sysconf(_SC_NPROCESSORS_ONLN);
  if (nb < 1) {
    fprintf(stderr, "[e] Could not determine number of CPUs\n");
    exit (EXIT_FAILURE);
  }

  return nb;
}

/**
 * get_delta_uptime_in_ms
 * return the number of milliseconds spent since the last time this method was
 * called. It uses the global variable `last_uptime`. 
 *
 * @param:
 *    void
 *
 * @return:
 *    long
 */
long get_delta_uptime_in_ms()
{
  long ms;
  time_t s;
  struct timespec spec;
  clock_gettime(CLOCK_REALTIME, &spec);

  s  = spec.tv_sec;
  ms = round(spec.tv_nsec / 1.0e6); // Convert nanoseconds to milliseconds
  ms += s * 1000; // Convert seconds to milliseconds and them to ms

  long delta = ms - last_uptime;
  last_uptime = ms;

  printf("[i] uptime: %ld\n", delta);

  return delta;
}

/**
 * open_file
 * open the file located at the path given in parameter and return its pointer
 *
 * @param:
 *    char* path
 *
 * @return:
 *    FILE*
 */
FILE* open_file(char* path)
{
  FILE* fp = fopen(path, "r");
  if(fp == NULL) {
    perror("[e] Fopen failed");
    fprintf(stderr, "[e] The given file path does not exist\n");
  }

  return fp;
}

/**
 * close_file
 * close the file given in parameter
 *
 * @param:
 *    FILE* fp
 *
 * @return:
 *    void
 */
void close_file(FILE* fp)
{
  if(fclose(fp) != 0)
    perror("[e] Fclose failed");
}

/**
 * read_stat_file
 * read the `stat` file given in the `path` parameter and fill the `delta`
 * struct with the difference between the new values and the `last` values
 *
 * @param:
 *    char* path
 *    struct cpu_stat* delta
 *    struct cpu_stat* last
 *
 * @return:
 *    void
 */
void read_stat_file(char* path, struct cpu_stat* delta, struct cpu_stat* last)
{
  int var; /* dummy variable for iterations */
  FILE* fp;
  struct cpu_stat* new;
  new = malloc(nb_cpus * sizeof(struct cpu_stat));

  /* Open the /stat file and read the values */
  fp = open_file(path);
  fscanf(fp, "%*[^\n]\n"); /* Jump the first line of /proc/stat */
  for(var = 0 ; var < nb_cpus ; var++) {
    fscanf(fp, "%*s %lf %lf %lf %lf %*f %*f %*f %*f %*f %*f",
      &new[var]._user, 
      &new[var]._nice, 
      &new[var]._system, 
      &new[var]._idle
    );
  }
  
  close_file(fp);

  /* Calculate the delta values */
  for(var = 0 ; var < nb_cpus ; var++) {
    delta[var]._user   = new[var]._user    - last[var]._user;
    delta[var]._nice   = new[var]._nice    - last[var]._nice;
    delta[var]._system = new[var]._system  - last[var]._system;
    delta[var]._idle   = new[var]._idle    - last[var]._idle;
  }

  /* Save the new values in the `last` structure */
  for(var = 0 ; var < nb_cpus ; var++) {
    last[var]._user    = new[var]._user;
    last[var]._nice    = new[var]._nice;
    last[var]._system  = new[var]._system;
    last[var]._idle    = new[var]._idle;
  }

  /* Print the values */
  for(var = 0 ; var < nb_cpus ; var++) {
    printf("[i] CPU%d\tuser:%3.0f\tnice:%3.0f\tsystem:%3.0f\tidle:%3.0f\n",
      var, delta[var]._user, delta[var]._nice,
      delta[var]._system, delta[var]._idle);
  }
}

/**
 * set_timer
 * initialize the value of the timer SIGALRM and define the callback method
 *
 * @param:
 *    void
 *
 * @return:
 *    void
 */
void set_timer()
{
  signal(SIGALRM, timer_handler);
  // ualarm(500000, 500000); /* after 100 ms, every 100 ms */
  alarm(1);
}

/**
 * timer_handler
 * callback method which is called by the internal timer every X milliseconds
 * it handles the calculation of the CPU usage by core for the whole system
 *
 * @param:
 *    int sig
 *
 * @return:
 *    static void
 */
static void timer_handler(int sig)
{
  int var; /* dummy variable for iterations */

  signal(SIGALRM, SIG_IGN); /* ignore this signal */
  printf("\n[i] Alarm triggered!\n");

  /* Get uptime */
  // long delta_uptime = get_delta_uptime_in_ms();

  /* Initialize the delta cpu struct */
  struct cpu_stat delta_cpu_stat[nb_cpus];

  /* Read CPU values */
  read_stat_file(path, delta_cpu_stat, last_cpu_stat);

  /* Calculate CPU usage by CPU */
  float cpu_usage[nb_cpus];
  memset(cpu_usage, 0, sizeof(cpu_usage));
  var = 0;
  while(var < nb_cpus) {
    double total_usage_time = delta_cpu_stat[var]._user + 
                              delta_cpu_stat[var]._nice + 
                              delta_cpu_stat[var]._system;
    double total_time_overall = total_usage_time + delta_cpu_stat[var]._idle;

    cpu_usage[var] = 100 * (total_usage_time / total_time_overall);

    var++;
  }

  for(var = 0 ; var < nb_cpus ; var++)
    printf("[i] CPU%d usage: %3.1f%%\n", var, cpu_usage[var]);

  set_timer();
}

/**
 * main
 * entry point of the program
 * 
 * @param:
 *    int argc
 *    char* argv[]
 *
 * @return:
 *    int
 */
int main(int argc, char* argv[])
{
  printf("[i] Calculate the CPU usage for the whole system\n");

  path = malloc(strlen("/proc/stat") +  1);
  strcpy(path, "/proc/stat");

  /* Get nb CPUs */
  nb_cpus = get_nb_of_cpus();

  /* Get hertz */
  hertz = sysconf(_SC_CLK_TCK);
  printf("[i] hertz  = %d\n", hertz);

  last_cpu_stat = malloc(nb_cpus * sizeof(struct cpu_stat));

  /* FIX ME: `last_cpu_stat` initialized at {0} */
  int var = 0;
  for(; var < nb_cpus ; var++) {
    last_cpu_stat[var]._user    = 0;
    last_cpu_stat[var]._nice    = 0;
    last_cpu_stat[var]._system  = 0;
    last_cpu_stat[var]._idle    = 0;
  }

  set_timer();

  while(1);

  return 0;
}