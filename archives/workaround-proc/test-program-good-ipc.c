/**
 * program which calculates alternately the multiplication and the addition of
 * double numbers
 */
#include <stdio.h>

#define SIZE 8

int main (void)
{
  int iteration1;
  int iteration2;
  const double nb1[SIZE] = {5};
  const double nb2[SIZE] = {3};
  double result1[SIZE];
  double result2[SIZE];
  int i, j, k;

  printf("Enter the number of iterations for loop 1: ");
  scanf("%d", &iteration1);
  printf("\n");
  printf("Enter the number of iterations for loop 2: ");
  scanf("%d", &iteration2);

  printf("iteration1=%d iteration2%d\n", iteration1, iteration2);
  
  for(i = 0; i < iteration1; i ++) {
#pragma SIMD
#pragma vector aligned
    for(j = 0; j < iteration2; j++) {
      for (k = 0; k < SIZE; k++) {
        result1[k] += nb1[k];
        result2[k] *= nb2[k];
      }
    }
  }

  return 1;
}