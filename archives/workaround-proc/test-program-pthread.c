#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

/*
 * Fibonacci
 */
int fibonacci(int n)
{
  if ( n == 0 )
    return 0;
  else if ( n == 1 )
    return 1;
  else
    return ( fibonacci(n-1) + fibonacci(n-2) );
}

static void* calculate_fibonacci()
{
  int n = 20, result = 0, i = 0, c;
  while(1) {
    for (c = 0 ; c <= n ; c++) {
      result += fibonacci(i);
      i++; 
    }
    // printf( "Fibonacci(%d) = %d\n", n, result );
    n += 5;
  }

  return NULL;
}

/*
 * Syracuse
 */
int syracuse(int n)
{
  int threshold = n-1;

  while (n > threshold) {
    if (n & 1)
      n = 3*n+1;
    else
      n >>= 1;
  }

  return 0;
}


static void* calculate_syracuse()
{
  unsigned long long n = 3;

  while (1) {
    syracuse(n);
    n+=4;
  }

  return NULL;
}

/*
 * Prime number
 */
int prime_number(int n)
{
  if(n <= 1) return 0;
  unsigned int i;
  for (i = 2 ; i < n ; i++) {
    if (n % i == 0)
      return 0;
  }
  // printf("[i] %d is a prime\n", n);
  return 1;
}

static void* calculate_prime_number()
{
  unsigned int n = 1;
  while(1) {
    prime_number(n++);
  }

  return NULL;
}

int main (void)
{
  int ret;
  pthread_t thread_fibo_store;
  pthread_t thread_syra_store;
  pthread_t thread_prim_store;

  /*
   * Create thread for Fibonacci
   */
  ret = pthread_create (
    &thread_fibo_store, NULL,
    calculate_fibonacci, NULL
  );

  if(ret != 0)
    fprintf(stderr, "[e] %s", strerror(ret));
  else
    printf("[i] Thread Fibonacci is created\n");

  /*
   * Create thread for Syracuse
   */
  ret = pthread_create (
    &thread_syra_store, NULL,
    calculate_syracuse, NULL
  );

  if(ret != 0)
    fprintf(stderr, "[e] %s", strerror(ret));
  else
    printf("[i] Thread Syracuse is created\n");

  /*
   * Create thread for Prime number
   */
  ret = pthread_create (
    &thread_prim_store, NULL,
    calculate_prime_number, NULL
  );

  if(ret != 0)
    fprintf(stderr, "[e] %s", strerror(ret));
  else
    printf("[i] Thread Prime number is created\n");

  printf("[i] Main is going to do stupid stuff to busy the CPU\n");
  while(1) {
    if(ret % 2) ret += 3;
    else ret = (ret * 2) - 1;
  }

  /* Wait until the end of each thread */
  pthread_join (thread_fibo_store, NULL);
  pthread_join (thread_syra_store, NULL);

  return EXIT_SUCCESS;
}