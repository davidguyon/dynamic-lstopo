/*
 * Copyright © 2014 David Guyon (david@guyon.me)
 */

#include "performance-tools.h"

char* path;

void init_cpu_usage_whole_system();
void init_cpu_usage_pid(int pid);
void update_cpu_usage_whole_system();
void update_cpu_usage_pid();

/**
 * init_cpu_usage
 * call the initialization method depending on the <pid> parameter (CPU usage
 * calculation on the whole system or just for the given <pid>)
 *
 * @param:
 *    int pid
 *
 * @return:
 *    void
 */
void init_cpu_usage(int pid)
{
  /*
   * Initialize CPU usage counters for the whole system
   */
  if(pid <= 0)
    init_cpu_usage_whole_system();

  /*
   * Initialize CPU usage counters for the given <pid>
   */
  else
    init_cpu_usage_pid(pid);
}

/**
 * update_cpu_usage
 * update the array of integers `cpu_usage` depending on the <pid> parameter
 * (CPU usage calculation on the whole system or just for the given <pid>)
 *
 * @param:
 *    int pid
 *
 * @return:
 *    void
 */
void update_cpu_usage(int pid)
{
  /*
   * Calculate the CPU usage on the whole system
   */
  if(pid <= 0)
    update_cpu_usage_whole_system();

  /*
   * Calculate the CPU usage for the given <pid>
   */
  else
    update_cpu_usage_pid();
}

/*******************************************************************************
 * PRIVATES METHODS
 */

/**
 * open_file
 * open the file located at the path given in parameter and return its pointer
 *
 * @param:
 *    char* path
 *
 * @return:
 *    FILE*
 */
FILE* open_file(char* path)
{
  FILE* fp = fopen(path, "r");
  if(fp == NULL) {
    perror("[e] Fopen failed");
    fprintf(stderr, "[e] The given file path does not exist\n");
  }

  return fp;
}

/**
 * close_file
 * close the file given in parameter
 *
 * @param:
 *    FILE* fp
 *
 * @return:
 *    void
 */
void close_file(FILE* fp)
{
  if(fclose(fp) != 0)
    perror("[e] Fclose failed");
}

/*******************************************************************************
 * CPU USAGE CALCULATION FOR THE WHOLE SYSTEM
 */

struct cpu_stat {
  double _user;
  double _nice;
  double _system;
  double _idle;
};

/*
 * Array of struct cpu_stat
 * Each row represents a CPU core and it contains the previous cpu_stat data
 */
struct cpu_stat* last_cpu_stat;

/**
 * read_proc_stat_file
 * read /proc/stat and calculate the delta values by comparing the new values
 * and the previous ones
 *
 * @param:
 *    struct cpu_stat* delta
 *
 * @return:
 *    void
 */
void read_proc_stat_file(struct cpu_stat* delta)
{
  int var; /* dummy variable for iterations */
  FILE* fp;
  struct cpu_stat* new;
  new = malloc(nb_cpu * sizeof(struct cpu_stat));

  /* Open `/proc/stat` file and read the values */
  fp = open_file("/proc/stat");
  int nb = 0;
  nb = fscanf(fp, "%*[^\n]\n"); /* Jump the first line */
  if(nb != 0)
    fprintf(stderr, "[e] fscanf returned a wrong value (should be 0 instead of %d)\n", nb);

  for(var = 0 ; var < nb_cpu ; var++) {
    nb = fscanf(fp, "%*s %lf %lf %lf %lf %*f %*f %*f %*f %*f %*f",
      &new[var]._user, 
      &new[var]._nice, 
      &new[var]._system, 
      &new[var]._idle
    );
    if(nb != 4)
      fprintf(stderr, "[e] fscanf returned a wrong value (should be 4 instead of %d)\n", nb);
  }
  close_file(fp);

  /* Calculate the delta values */
  for(var = 0 ; var < nb_cpu ; var++) {
    delta[var]._user   = new[var]._user   - last_cpu_stat[var]._user;
    delta[var]._nice   = new[var]._nice   - last_cpu_stat[var]._nice;
    delta[var]._system = new[var]._system - last_cpu_stat[var]._system;
    delta[var]._idle   = new[var]._idle   - last_cpu_stat[var]._idle;
  }

  /* Save the new values in the `last_cpu_stat` structure */
  for(var = 0 ; var < nb_cpu ; var++) {
    last_cpu_stat[var]._user   = new[var]._user;
    last_cpu_stat[var]._nice   = new[var]._nice;
    last_cpu_stat[var]._system = new[var]._system;
    last_cpu_stat[var]._idle   = new[var]._idle;
  }
}

/**
 * init_cpu_usage_whole_system
 * initialize the CPU usage counter to count the performance on the whole system
 *
 * @param:
 *    void
 *
 * @return:
 *    void
 */
void init_cpu_usage_whole_system()
{
  /* Initialize the `last_cpu_stat` array for the first use */
  /* FIX ME: `last_cpu_stat` initialized at {0} */
  last_cpu_stat = malloc(nb_cpu * sizeof(struct cpu_stat));
  int var = 0;
  for(; var < nb_cpu ; var++) {
    last_cpu_stat[var]._user    = 0;
    last_cpu_stat[var]._nice    = 0;
    last_cpu_stat[var]._system  = 0;
    last_cpu_stat[var]._idle    = 0;
  }
}

/**
 * udpate_cpu_usage_whole_system
 * update the `cpu_usage` field of each struct counter_by_pu with the result of
 * the final calculation of the CPU usage on each PU on the whole system
 *
 * @param:
 *    void
 *
 * @return:
 *    void
 */
void update_cpu_usage_whole_system()
{
  int var; /* dummy variable for iterations */

  /* Initialize the delta cpu struct */
  struct cpu_stat delta_cpu_stat[nb_cpu];

  read_proc_stat_file(delta_cpu_stat);

  /* Calculate CPU usage by CPU */
  var = 0;
  for(var = 0 ; var < nb_cpu ; var++) {
    double total_usage_time = delta_cpu_stat[var]._user + 
                              delta_cpu_stat[var]._nice + 
                              delta_cpu_stat[var]._system;
    double total_time_overall = total_usage_time + delta_cpu_stat[var]._idle;

    /* Avoid division by zero */
    int cpu_usage = 0;
    if(total_time_overall != 0)
      cpu_usage = 100 * (total_usage_time / total_time_overall);

    if(cpu_usage > 100) {
      printf("[w] CPU usage exceeded 100%% (reset to 100%%)\n");
      cpu_usage = 100;
    }

    /* Update the corresponding `cpu_usage` field in the struct counter_by_pu */
    int i, j;
    for(i = 0 ; i < nb_core ; i++)
    {
      for(j = 0 ; j < nb_pu_by_core ; j++)
      {
        int index = counter_by_core[i].counter_by_pu[j].pu_index;
        if(index == var)
        {
          counter_by_core[i].counter_by_pu[j].cpu_usage = cpu_usage;
          printf("[i] Core%d PU%d usage: %3d%%\n", i, j, cpu_usage);
        }
      }
    }
  }
}

/*******************************************************************************
 * CPU USAGE CALCULATION FOR A SPECIFIC <pid>
 */

struct stat {
  int processor;
  long int cutime;
  long int cstime;
  long unsigned int utime;
  long unsigned int stime;
};

int hertz;
int nb_tasks;
long last_uptime;
char pid_char[10] = "";

/*
 * Array of struct stat
 * Each row represents a CPU core and it contains the previous stat data (utime,
 * stime, cutime, cstime and used processor)
 */
struct stat* last_stat;

/**
 * convert_pid_to_char
 * the given integer <pid> is converted into an array of char in `pid_char`
 *
 * @param:
 *    int pid
 *
 * @return:
 *    void
 */
void convert_pid_to_char(int pid)
{
  snprintf(pid_char, sizeof(pid_char), "%d", pid);
}

/**
 * get_nb_of_tasks
 * return the number of files in the /proc/<pid>/task/ folder
 * the global `path` variable (char*) needs to be initialize accordingly
 * 
 * @param:
 *    void
 *
 * @return:
 *    int
 */
int get_nb_of_tasks()
{
  DIR* dir;
  int nb = 0;
  struct dirent *entry;

  dir = opendir(path);
  while ((entry = readdir (dir)) != NULL) {
    if( /* ignore . and .. */
      !strcmp(entry->d_name, ".") ||
      !strcmp(entry->d_name, "..")) {
      continue;
    }

    nb++;
  }
  closedir(dir);

  return nb;
}

/**
 * get_delta_uptime_in_ms
 * return the number of milliseconds spent since the last time this method was
 * called
 * the global variable `last_uptime` (long) needs to exist and will be updated
 * with the new uptime value at the end of this method
 *
 * @param:
 *    void
 *
 * @return:
 *    long
 */
long get_delta_uptime_in_ms()
{
  long ms;
  time_t s;
  struct timespec spec;

  /* Get uptime from the real-time clock */
  clock_gettime(CLOCK_REALTIME, &spec);

  /* Convert nanoseconds to milliseconds
   * and seconds to milliseconds
   * and finally calculate the total amount of milliseconds */
  s  = spec.tv_sec;
  ms = round(spec.tv_nsec / 1.0e6);
  ms += (s * 1000);

  long delta = ms - last_uptime;

  /* Update the global variable `last_uptime` with the new uptime value */
  last_uptime = ms;

  return delta;
}

/**
 * read_stat_file
 * read the `stat` file of the <pid>/<tid> given in the `file` parameter and
 * fill the `delta` struct with the difference between the new values and the
 * `last` values
 *
 * @param:
 *    char* file
 *    struct stat* delta
 *    struct stat* last
 *
 * @return:
 *    void
 */
void read_stat_file(char* file, struct stat* delta, struct stat* last)
{
  FILE* fp;
  char* path_stat;
  struct stat new = {0};

  /* Initialize the path to the /proc/<pid>/task/<tid>/stat file */
  path_stat = malloc(strlen(path) + strlen(file) + strlen("/stat") +  1);
  strcpy(path_stat, path);
  strcat(path_stat, file);
  strcat(path_stat, "/stat");

  /* Open the stat file and read the values */
  fp = open_file(path_stat);
  int nb = 0;
  nb = fscanf(fp, 
    "%*d %*s %*c %*d %*d %*d %*d %*d %*u %*u %*u %*u %*u %lu %lu %ld %ld %*d"
    "%*d %*d %*d %*u %*u %*d %*u %*u %*u %*u %*u %*u %*u %*u %*u %*u %*u %*u"
    "%*u %*d %d", 
    &new.utime, 
    &new.stime, 
    &new.cutime, 
    &new.cstime,
    &new.processor
  );
  
  if(nb != 5) {
    fprintf(stderr, "[e] fscanf returned a wrong value (should be 5 instead of %d)\n", nb);
    fprintf(stderr, "    Path: %s\n", path_stat);
  }

  close_file(fp);

  /* Calculate the delta values */
  delta->utime     = new.utime  - last->utime;
  delta->stime     = new.stime  - last->stime;
  delta->cutime    = new.cutime - last->cutime;
  delta->cstime    = new.cstime - last->cstime;
  delta->processor = new.processor;

  /* Save the new values in the _last_ structure */
  last->utime     = new.utime;
  last->stime     = new.stime;
  last->cutime    = new.cutime;
  last->cstime    = new.cstime;
  last->processor = new.processor;
}

/**
 * init_cpu_usage_pid
 * initialize the CPU usage counter to count the performance on the given <pid>
 *
 * @param:
 *    int pid
 *
 * @return:
 *    void
 */
void init_cpu_usage_pid(int pid)
{
  /* Get hertz */
  hertz = sysconf(_SC_CLK_TCK);
  printf("[i] %d hertz\n", hertz);

  /* Get path depending on the pid */
  convert_pid_to_char(pid);
  path = malloc(strlen("/proc/") + strlen(pid_char) + strlen("/task/") +  1);
  strcpy(path, "/proc/");
  strcat(path, pid_char);
  strcat(path, "/task/");

  nb_tasks = get_nb_of_tasks();

  /* Initialize the `last_stat` array for the first use */
  /* FIX ME: _last_stat_ initialized at {0} */
  last_stat = malloc(nb_tasks * sizeof(struct stat));
  int var = 0;
  for(; var < nb_tasks ; var++) {
    last_stat[var].utime     = 0;
    last_stat[var].stime     = 0;
    last_stat[var].cutime    = 0;
    last_stat[var].cstime    = 0;
    last_stat[var].processor = 0;
  }
}

/**
 * update_cpu_usage_pid
 * update the `cpu_usage` field of each struct counter_by_pu with the result of
 * the final calculation of the CPU usage on each PU for a specific <pid>
 *
 * @param:
 *    void
 *
 * @return:
 *    void
 */
void update_cpu_usage_pid()
{
  int var; /* dummy variable for iterations */

  /* Get uptime */
  long delta_uptime = get_delta_uptime_in_ms();

  /* Initialize the delta_stat struct (contains <pid> + all <tid>) */
  struct stat delta_stat[nb_tasks];

  /* Read all <tid>/stat */
  DIR* dir;
  struct dirent *entry;
  dir = opendir(path);
  var = 0;
  while ((entry = readdir(dir)) != NULL) {
    if( /* ignore . and .. */
      !strcmp(entry->d_name, ".") ||
      !strcmp(entry->d_name, "..")) {
      continue;
    }

    read_stat_file(entry->d_name, &delta_stat[var], &last_stat[var]);

    var++;
  }
  closedir(dir);

  /* Calculate process duration by CPU */
  long unsigned int process_duration[nb_cpu];
  memset(process_duration, 0, sizeof(process_duration));  
  var = 0;
  while(var < (nb_tasks)) {
    int cpu = delta_stat[var].processor;
    process_duration[cpu] += delta_stat[var].utime;
    process_duration[cpu] += delta_stat[var].stime;
    process_duration[cpu] += delta_stat[var].cutime;
    process_duration[cpu] += delta_stat[var].cstime;
    var++;
  }

  /* Calculate CPU usage by core and update the corresponding `cpu_usage` field
   * in the struct counter_by_pu */
  for(var = 0 ; var < nb_cpu ; var++)
  {
    float a = (float)process_duration[var] / (float)hertz;
    int cpu_usage = (int) (100 * ( a / ((float)delta_uptime/1000) ));

    if(cpu_usage > 100) {
      printf("[w] CPU usage exceeded 100%% (reset to 100%%)\n");
      cpu_usage = 100;
    }

    int i, j;
    for(i = 0 ; i < nb_core ; i++)
    {
      for(j = 0 ; j < nb_pu_by_core ; j++)
      {
        int index = counter_by_core[i].counter_by_pu[j].pu_index;
        if( index == var )
        {
          counter_by_core[i].counter_by_pu[j].cpu_usage = cpu_usage;
          printf("[i] Core %d PU %d usage: %3d%%\n", i, j, cpu_usage);
        }
      }
    }
  }
}