/*
 * Copyright © 2014 David Guyon (david@guyon.me)
 */

#include "performance-tools.h"

/**
 * draw_cpu_usage_on_pu_box
 * draw a box on the PU box displaying the CPU usage data by the height of the
 * box going up and down and the IPC data by a background color going from
 * yellow to red
 *
 * @param:
 *    struct draw_methods* methods
 *    void* output
 *    int x
 *    int y
 *    int width
 *    int max_height
 *    int depth
 *    int cpu_number
 *
 * @return:
 *    void
 */
void draw_cpu_usage_on_pu_box(
  struct draw_methods* methods, void* output,
  int x, int y, int width, int max_height,
  int depth, int cpu_number
){

  /* Get the cpu usage data and ipc data */
  int cpu_usage = 0;
  int ipc = 0;
  int i, j;
  for(i = 0 ; i < nb_core ; i++)
  {
    for(j = 0 ; j < nb_pu_by_core ; j++)
    {
      int index = counter_by_core[i].counter_by_pu[j].pu_index;
      if(index == cpu_number)
      {
        cpu_usage = counter_by_core[i].counter_by_pu[j].cpu_usage;
        ipc = counter_by_core[i].counter_by_pu[j].ipc;
      }
    }
  }

  /* Calculate height depending on CPU usage */
  int height = cpu_usage * max_height / 100;
  
  /* Calculate bottom position */
  y = y + max_height - height;

  if(ipc != -1)
    methods->box(output, 255, ipc, 0, depth-1, x, width, y, height);
  else
    methods->box(output, 196, 196, 196, depth-1, x, width, y, height);
}

/**
 * draw_cache_info_on_cache_box
 * draw a box on the cache box displaying the cache usage (current cache use by
 * max cache use hit so far) by the height of the box going up and down and the
 * cache miss ratio (miss over access) by a background color going from white
 * gray to dark blue
 *
 * @param:
 *    hwloc_obj_t cache_obj
 *    struct draw_methods* methods
 *    void* output
 *    int x
 *    int y
 *    int width
 *    int max_height
 *    int depth
 *
 * @return:
 *    void
 */
void draw_cache_info_on_cache_box(
  hwloc_obj_t cache_obj, struct draw_methods* methods,
  void* output, int x, int y, 
  int width, int max_height, int depth
){
  float ratio = -1;
  float cache_usage = -1;
  int r = 0, g = 0, b = 0;
  int blue_intensity = 0;
  int core = cache_obj->logical_index;
  int cache_level = cache_obj->attr->cache.depth;
  
  /* Calculate blue intensity */
  switch(cache_level) {

    /* Cache L1 */
    case 1:
      switch(cache_obj->attr->cache.type) {
        case HWLOC_OBJ_CACHE_UNIFIED:
          printf("[e] Cache L1 unified (neither DATA or INSTRUCTION) WAT?\n");
        break;

        /* Cache L1 DATA */
        case HWLOC_OBJ_CACHE_DATA:
          if(counter_by_core[core].cache_L1D_ratio != -1) {
            ratio = (float)counter_by_core[core].cache_L1D_ratio / 100;
            //ratio = (float)counter_by_core[core].cache_L1D_miss /
            //        (float)counter_by_core[core].cache_L1D_max_miss;
            cache_usage = (float)counter_by_core[core].cache_L1D_access /
                          (float)counter_by_core[core].cache_L1D_max_access;
            blue_intensity = ((int) 255 * ratio);
            b = 255;
            r = 255 - blue_intensity;
            g = 255 - blue_intensity;
          } else {
            b = 196;
            r = 196;
            g = 196;
          }
        break;

        /* Cache L1 INSTRUCTION */
        case HWLOC_OBJ_CACHE_INSTRUCTION:
          if(counter_by_core[core].cache_L1I_ratio != -1) {
            ratio = (float)counter_by_core[core].cache_L1I_ratio / 100;
            //ratio = (float)counter_by_core[core].cache_L1I_miss /
            //        (float)counter_by_core[core].cache_L1I_max_miss;
            cache_usage = (float)counter_by_core[core].cache_L1I_access /
                          (float)counter_by_core[core].cache_L1I_max_access;
            blue_intensity = ((int) 255 * ratio);
            b = 255;
            r = 255 - blue_intensity;
            g = 255 - blue_intensity;
          } else {
            b = 196;
            r = 196;
            g = 196;
          }
        break;
      }
    break;

    /* Cache L2 */
    case 2:
      if(counter_by_core[core].cache_L2_ratio != -1 && cache_L2_activated) {
        ratio = (float)counter_by_core[core].cache_L2_ratio / 100;
        //ratio = (float)counter_by_core[core].cache_L2_miss /
        //        (float)counter_by_core[core].cache_L2_max_miss;
        cache_usage = (float)counter_by_core[core].cache_L2_access /
                      (float)counter_by_core[core].cache_L2_max_access;
        blue_intensity = ((int) 255 * ratio);
        b = 255;
        r = 255 - blue_intensity;
        g = 255 - blue_intensity;
      } else {
        b = 196;
        r = 196;
        g = 196;
      }
    break;

    /* Cache L3 */
    case 3:
      if(cache_L3_ratio != -1) {
        ratio = (float)cache_L3_ratio / 100;
        //ratio = (float)cache_L3_miss / (float)cache_L3_max_miss;
        cache_usage = (float)cache_L3_access / (float)cache_L3_max_access;
        blue_intensity = ((int) 255 * ratio);
        b = 255;
        r = 255 - blue_intensity;
        g = 255 - blue_intensity;
      } else {
        b = 196;
        r = 196;
        g = 196;
      }
    break;

    default:
      fprintf(stderr, "[e] Cannot handle the cache level %d\n", cache_level);
  }

  if(ratio != -1 && cache_usage != -1)
  {
    if(cache_usage > 1) cache_usage = 1;

    /* Calculate height depending on cache usage */
    int height = cache_usage * max_height;

    if(height > max_height) height = max_height;

    /* Calculate bottom position */
    y = y + max_height - height;
  
    methods->box(output, r, g, b-10, depth-1, x, width, y, height);
  }
}
