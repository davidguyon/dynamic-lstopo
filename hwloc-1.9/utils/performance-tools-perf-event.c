/*
 * Copyright © 2014 David Guyon (david@guyon.me)
 */

#include "performance-tools.h"

/*
 * sycall configuration
 */
pid_t pid;
const int grp = -1;
const int flags = 0;

/*
 * struct perf_event_attr configuration
 */
const int pinned = 0;
const int inherit = 1;
const int disabled = 0;
const int exclude_hv = 0;
const int exclude_idle = 0;
const int exclude_kernel = 0;
const int enable_on_exec = 0;
const int read_format = PERF_FORMAT_TOTAL_TIME_ENABLED
                      | PERF_FORMAT_TOTAL_TIME_RUNNING;

/*
 * struct for the counter result
 */
struct read_format_struct
{
  uint64_t value;         /* The value of the event */
  uint64_t time_enabled;  /* if PERF_FORMAT_TOTAL_TIME_ENABLED */
  uint64_t time_running;  /* if PERF_FORMAT_TOTAL_TIME_RUNNING */
};

int open_fd(int type, int config, int cpu);
uint64_t read_fd(struct counter* c);
void init_counter(int type, int config, char* name, struct counter* c, int cpu);
void update_counter(struct counter* c);
void print_counter(struct counter* c, bool pipe);

/**
 * init_perf_event_counters
 * initialize the performance counters in the array of struct counter_by_core
 * depending on the number of Core and the number of PU by Core
 *
 * @param:
 *    int _pid
 *
 * @return:
 *    void
 */
void init_perf_event_counters(int _pid)
{
  pid = _pid;

  int i, j;
  int depth = hwloc_get_type_depth(local_topology, HWLOC_OBJ_CORE);

  for(i = 0 ; i < nb_core ; i++)
  {
    printf("[i] Core %d\n", i);
    hwloc_obj_t obj = hwloc_get_obj_by_depth(local_topology, depth, i);

    for(j = 0 ; j < nb_pu_by_core ; j++)
    {
      /* Save the os_index in the pu_index field */
      counter_by_core[i].counter_by_pu[j].pu_index = obj->children[j]->os_index;
      
      printf("[i] PU %d\n", counter_by_core[i].counter_by_pu[j].pu_index);

      init_counter(PERF_TYPE_HW_CACHE,
      (PERF_COUNT_HW_CACHE_LL) |
      (PERF_COUNT_HW_CACHE_OP_READ << 8) |
      (PERF_COUNT_HW_CACHE_RESULT_MISS << 16),
      "CACHE L3 MISS", &counter_by_core[i].counter_by_pu[j].CACHE_L3_MISS,
      counter_by_core[i].counter_by_pu[j].pu_index);

      init_counter(PERF_TYPE_HW_CACHE,
      (PERF_COUNT_HW_CACHE_LL) |
      (PERF_COUNT_HW_CACHE_OP_READ << 8) |
      (PERF_COUNT_HW_CACHE_RESULT_ACCESS << 16),
      "CACHE L3 ACCESS", &counter_by_core[i].counter_by_pu[j].CACHE_L3_ACCESS,
      counter_by_core[i].counter_by_pu[j].pu_index);

      if(cache_L2_activated)
      {
        init_counter(PERF_TYPE_RAW,
        cache_L2_miss_config,
        "CACHE L2 MISS", &counter_by_core[i].counter_by_pu[j].CACHE_L2_MISS,
        counter_by_core[i].counter_by_pu[j].pu_index);

        init_counter(PERF_TYPE_RAW,
        cache_L2_access_config,
        "CACHE L2 ACCESS", &counter_by_core[i].counter_by_pu[j].CACHE_L2_ACCESS,
        counter_by_core[i].counter_by_pu[j].pu_index);
      }

      init_counter(PERF_TYPE_HW_CACHE,
      (PERF_COUNT_HW_CACHE_L1D) |
      (PERF_COUNT_HW_CACHE_OP_READ << 8) |
      (PERF_COUNT_HW_CACHE_RESULT_MISS << 16),
      "CACHE L1D MISS", &counter_by_core[i].counter_by_pu[j].CACHE_L1D_MISS,
      counter_by_core[i].counter_by_pu[j].pu_index);

      init_counter(PERF_TYPE_HW_CACHE,
      (PERF_COUNT_HW_CACHE_L1D) |
      (PERF_COUNT_HW_CACHE_OP_READ << 8) |
      (PERF_COUNT_HW_CACHE_RESULT_ACCESS << 16),
      "CACHE L1D ACCESS", &counter_by_core[i].counter_by_pu[j].CACHE_L1D_ACCESS,
      counter_by_core[i].counter_by_pu[j].pu_index);

      init_counter(PERF_TYPE_HW_CACHE,
      (PERF_COUNT_HW_CACHE_L1I) |
      (PERF_COUNT_HW_CACHE_OP_READ << 8) |
      (PERF_COUNT_HW_CACHE_RESULT_MISS << 16),
      "CACHE L1I MISS", &counter_by_core[i].counter_by_pu[j].CACHE_L1I_MISS,
      counter_by_core[i].counter_by_pu[j].pu_index);

      init_counter(PERF_TYPE_HW_CACHE,
      (PERF_COUNT_HW_CACHE_L1I) |
      (PERF_COUNT_HW_CACHE_OP_READ << 8) |
      (PERF_COUNT_HW_CACHE_RESULT_ACCESS << 16),
      "CACHE L1I ACCESS", &counter_by_core[i].counter_by_pu[j].CACHE_L1I_ACCESS,
      counter_by_core[i].counter_by_pu[j].pu_index);

      init_counter(PERF_TYPE_HARDWARE, PERF_COUNT_HW_CPU_CYCLES,
      "CPU CYCLES", &counter_by_core[i].counter_by_pu[j].CPU_CYCLES,
      counter_by_core[i].counter_by_pu[j].pu_index);

      init_counter(PERF_TYPE_HARDWARE, PERF_COUNT_HW_INSTRUCTIONS,
      "INSTRUCTIONS", &counter_by_core[i].counter_by_pu[j].INSTRUCTIONS,
      counter_by_core[i].counter_by_pu[j].pu_index);
    }
  }
}

/**
 * update_perf_event_counters
 * update the value of all initialized perf_event counters and calculate the IPC
 *
 * @param:
 *    void
 *
 * @return:
 *    void
 */
void update_perf_event_counters()
{
  int i, j;
  uint64_t sum_L3_miss = 0, sum_L3_access = 0;
  for(i = 0 ; i < nb_core ; i++)
  {
    for(j = 0 ; j < nb_pu_by_core ; j++)
    {
      update_counter(&counter_by_core[i].counter_by_pu[j].CPU_CYCLES);
      update_counter(&counter_by_core[i].counter_by_pu[j].INSTRUCTIONS);
      update_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L1I_MISS);
      update_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L1I_ACCESS);
      update_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L1D_MISS);
      update_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L1D_ACCESS);
      if(cache_L2_activated)
      {
        update_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L2_MISS);
        update_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L2_ACCESS);
      }
      update_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L3_MISS);
      update_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L3_ACCESS);

      /*
       * IPC calculation
       */
      if(
        counter_by_core[i].counter_by_pu[j].INSTRUCTIONS.value != 0 &&
        counter_by_core[i].counter_by_pu[j].CPU_CYCLES.value != 0
      ){
        counter_by_core[i].counter_by_pu[j].ipc = 
        ((float) counter_by_core[i].counter_by_pu[j].INSTRUCTIONS.value) /
        ((float) counter_by_core[i].counter_by_pu[j].CPU_CYCLES.value) * 100;
      } else {
        counter_by_core[i].counter_by_pu[j].ipc = -1;
      }

      /*
       * L3 cache miss/access calculations
       */
      sum_L3_miss += counter_by_core[i].counter_by_pu[j].CACHE_L3_MISS.value;
      sum_L3_access += counter_by_core[i].counter_by_pu[j].CACHE_L3_ACCESS.value;
    }

    /*
     * L1D and L1I cache miss/access calculations
     */
    uint64_t sum_L1D_miss = 0, sum_L1D_access = 0;
    uint64_t sum_L1I_miss = 0, sum_L1I_access = 0;

    for(j = 0 ; j < nb_pu_by_core ; j++)
    {
      sum_L1D_miss += counter_by_core[i].counter_by_pu[j].CACHE_L1D_MISS.value;
      sum_L1I_miss += counter_by_core[i].counter_by_pu[j].CACHE_L1I_MISS.value;
      sum_L1D_access += counter_by_core[i].counter_by_pu[j].CACHE_L1D_ACCESS.value;
      sum_L1I_access += counter_by_core[i].counter_by_pu[j].CACHE_L1I_ACCESS.value;
    }

    if(sum_L1D_access != 0 && sum_L1D_miss != 0) {
      counter_by_core[i].cache_L1D_ratio = 
        ((int)100 * ((float)sum_L1D_miss / sum_L1D_access));
    } else {
      counter_by_core[i].cache_L1D_ratio = -1;
    }

    if(sum_L1I_access != 0 && sum_L1I_miss != 0) {
      counter_by_core[i].cache_L1I_ratio = 
        ((int)100 * ((float)sum_L1I_miss / sum_L1I_access));
    } else {
      counter_by_core[i].cache_L1I_ratio = -1;
    }

    /* Save the number of L1D/L1I cache miss and update the max. value if needed */
    counter_by_core[i].cache_L1D_miss = sum_L1D_miss;
    counter_by_core[i].cache_L1I_miss = sum_L1I_miss;
    if(sum_L1D_miss > counter_by_core[i].cache_L1D_max_miss)
      counter_by_core[i].cache_L1D_max_miss = sum_L1D_miss;
    if(sum_L1I_miss > counter_by_core[i].cache_L1I_max_miss)
      counter_by_core[i].cache_L1I_max_miss = sum_L1I_miss;
    /* Save the number of L1D/L1I cache access and update the max. value if needed */
    counter_by_core[i].cache_L1D_access = sum_L1D_access;
    counter_by_core[i].cache_L1I_access = sum_L1I_access;
    if(sum_L1D_access > counter_by_core[i].cache_L1D_max_access)
      counter_by_core[i].cache_L1D_max_access = sum_L1D_access;
    if(sum_L1I_access > counter_by_core[i].cache_L1I_max_access)
      counter_by_core[i].cache_L1I_max_access = sum_L1I_access;

    if(cache_L2_activated)
    {
      /*
       * L2 cache miss/access calculations
       */
      uint64_t sum_L2_miss = 0, sum_L2_access = 0;

      for(j = 0 ; j < nb_pu_by_core ; j++)
      {
        sum_L2_miss += counter_by_core[i].counter_by_pu[j].CACHE_L2_MISS.value;
        sum_L2_access += counter_by_core[i].counter_by_pu[j].CACHE_L2_ACCESS.value;
      }

      if(sum_L2_access != 0 && sum_L2_miss != 0) {
        counter_by_core[i].cache_L2_ratio = 
          ((int)100 * ((float)sum_L2_miss / sum_L2_access));
      } else {
        counter_by_core[i].cache_L2_ratio = -1;
      }

      /* Save the number of L2 cache miss and update the max. value if needed */
      counter_by_core[i].cache_L2_miss = sum_L2_miss;
      if(sum_L2_miss > counter_by_core[i].cache_L2_max_miss)
        counter_by_core[i].cache_L2_max_miss = sum_L2_miss;
      /* Save the number of L2 cache access and update the max. value if needed */
      counter_by_core[i].cache_L2_access = sum_L2_access;
      if(sum_L2_access > counter_by_core[i].cache_L2_max_access)
        counter_by_core[i].cache_L2_max_access = sum_L2_access;
      }
  }

  /*
   * L3 cache miss/access ratio calculation
   */
  if(sum_L3_access != 0 && sum_L3_miss != 0) {
    cache_L3_ratio = ((int)100 * ((float)sum_L3_miss / sum_L3_access));
  } else {
    cache_L3_ratio = -1;
  }
  /* Save the number of L3 cache miss and update the max. value if needed */
  cache_L3_miss = sum_L3_miss;
  if(cache_L3_miss > cache_L3_max_miss)
    cache_L3_max_miss = cache_L3_miss;
  /* Save the number of L3 cache access and update the max. value if needed */
  cache_L3_access = sum_L3_access;
  if(cache_L3_access > cache_L3_max_access)
    cache_L3_max_access = cache_L3_access;
}

/*******************************************************************************
 * PRIVATES METHODS
 */

/**
 * init_counter
 * initialize the given counter struct with its counter configuration and the 
 * file descriptor pointing to the counter
 * 
 * @param:
 *    int type
 *    int config
 *    char* name
 *    struct counter* c
 *    int cpu
 *
 * @return:
 *    void
 */
void init_counter(int type, int config, char* name, struct counter* c, int cpu)
{
  c->value = 0;
  c->name = name;
  c->type = type;
  c->max_value = 0;
  c->last_value = 0;
  c->config = config;
  c->last_read_value = 0;
  c->last_read_time_enabled = 0;
  c->last_read_time_running = 0;
  c->file_descriptor = open_fd(type, config, cpu);

  if(c->file_descriptor == -1) {
    fprintf(stderr, "[e] Failed to catch the counter '%s'\n", c->name);

    if(!strcmp(c->name, "CPU CYCLES")) {
      fprintf(stderr, "[e] The IPC calculation is disabled\n");
    } else if(!strcmp(c->name, "INSTRUCTIONS")) {
      fprintf(stderr, "[e] The IPC calculation is disabled\n");
    } else if(!strcmp(c->name, "CACHE L1D MISS")) {
      fprintf(stderr, "[e] The L1D cache ratio calculation is disabled\n");
    } else if(!strcmp(c->name, "CACHE L1D ACCESS")) {
      fprintf(stderr, "[e] The L1D cache ratio calculation is disabled\n");
    } else if(!strcmp(c->name, "CACHE L1I MISS")) {
      fprintf(stderr, "[e] The L1I cache ratio calculation is disabled\n");
    } else if(!strcmp(c->name, "CACHE L1I ACCESS")) {
      fprintf(stderr, "[e] The L1I cache ratio calculation is disabled\n");
    } else if(!strcmp(c->name, "CACHE L2 MISS")) {
      fprintf(stderr, "[e] The L2 cache ratio calculation is disabled\n");
    } else if(!strcmp(c->name, "CACHE L2 ACCESS")) {
      fprintf(stderr, "[e] The L2 cache ratio calculation is disabled\n");
    } else if(!strcmp(c->name, "CACHE L3 MISS")) {
      fprintf(stderr, "[e] The L3 cache ratio calculation is disabled\n");
    } else if(!strcmp(c->name, "CACHE L3 ACCESS")) {
      fprintf(stderr, "[e] The L3 cache ratio calculation is disabled\n");
    } else {
      fprintf(stderr, "[e] This counter does not exist\n");
    }
  }
  else
    printf("[i] Counter '%s' initialized\n", c->name);
}

/**
 * update_counter
 * update the value of the counter specified in parameter
 *
 * @param:
 *    struct counter* c
 *
 * @return:
 *    void
 */
void update_counter(struct counter* c)
{
  c->last_value = c->value;
  c->value = read_fd(c);

  if(c->value > c->max_value)
    c->max_value = c->value;
}

/**
 * open_fd
 * make a system call to open a counter on a specific CPU depending on the
 * counter type and the counter configuration given in parameters
 *
 * @param:
 *    int type
 *    int config
 *    int cpu
 *
 * @return:
 *    int file descriptor of the counter or -1 on error
 */
int open_fd(int type, int config, int cpu)
{
  int file_descriptor;
  struct perf_event_attr events = {0};

  events.type = type;
  events.config = config;
  events.pinned = pinned;
  events.inherit = inherit;
  events.disabled = disabled;
  events.exclude_hv = exclude_hv;
  events.read_format = read_format;
  events.exclude_idle = exclude_idle;
  events.exclude_kernel = exclude_kernel;
  events.enable_on_exec = enable_on_exec;

  file_descriptor = syscall(
    __NR_perf_counter_open, 
    &events, 
    pid, 
    cpu, 
    grp, 
    flags
  );

  if (file_descriptor < 0 && file_descriptor > -4096) {
    if(errno == EACCES) {
      fprintf(stderr, "[e] When mesuring performance on the whole system, root "
        "permissions are needed\n");
      fprintf(stderr, "[e] Restart the application with root permissions\n");
      exit(0);
    }
    else if(errno == ESRCH) {
      fprintf(stderr, "[e] The given <pid> does not correspond to any existing "
        "process\n");
      fprintf(stderr, "[e] Restart the application with an existing <pid>\n");
      exit(0);
    }
    else {
      perror("[e] Syscall failed");
      return -1;
    }
  }

  return file_descriptor;
}

/**
 * read_fd
 * read the value kept in the file descriptor given in parameter
 *
 * @param:
 *    struct counter* c
 *
 * @return:
 *    uint64_t value of the counter or 0 on error
 */
uint64_t read_fd(struct counter* c)
{
  int length;
  struct read_format_struct result;

  if(c->file_descriptor == -1) {
    return 0;
  }

  length = read(c->file_descriptor, &result, sizeof(result));

  /* Reset the counter */
  ioctl(c->file_descriptor, PERF_EVENT_IOC_RESET, 0);

  /* Error handling */
  if(length != sizeof(result)) {
    fprintf(stderr, KRED "[e] Read failed for counter %s: " RESET "%s\n",
      c->name, strerror(errno));
    errno = 0;
    return 0;
  }

  /* Save the values just in case.. */
  c->last_read_value = result.value;
  c->last_read_time_enabled = result.time_enabled;
  c->last_read_time_running = result.time_running;

  /* Avoid division by zero */
  if(result.time_running == 0) return 0;

  float ratio = ((float) result.time_enabled) / ((float) result.time_running);
  return ((uint64_t) ((float) result.value) * ratio);
}

/**
 * print_counters
 * print the value of each counter of each CPU core in the output console
 *
 * @param:
 *    void
 *
 * @return:
 *    void
 */
void print_counters()
{
  int i, j;

  for(i = 0 ; i < 60 ; i++)
    printf("%s", HL);
  printf("\n");
  printf("[i] Counters value:\n");

  /* Enable the ' flag in printf for comma separators */
  setlocale(LC_NUMERIC, "");
  
  for(i = 0 ; i < nb_core ; i++)
  {
    printf("    Core %d\n", i);
    printf("    " VL " L1D ratio " KGRN "%d%%\n" RESET, 
      counter_by_core[i].cache_L1D_ratio);
    printf("    " VL " L1I ratio " KGRN "%d%%\n" RESET, 
      counter_by_core[i].cache_L1I_ratio);
    
    if(cache_L2_activated)
      printf("    " VL " L2  ratio " KGRN "%d%%\n" RESET, 
        counter_by_core[i].cache_L2_ratio);

    for(j = 0 ; j < nb_pu_by_core ; j++)
    {
      if(j != (nb_pu_by_core - 1))
      {
        printf("    " LC HL "PU %d\n", counter_by_core[i].counter_by_pu[j].pu_index);
        print_counter(&counter_by_core[i].counter_by_pu[j].CPU_CYCLES,true);
        print_counter(&counter_by_core[i].counter_by_pu[j].INSTRUCTIONS, true);
        print_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L1D_MISS, true);
        print_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L1D_ACCESS, true);
        print_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L1I_MISS, true);
        print_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L1I_ACCESS, true);
        if(cache_L2_activated)
        {
          print_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L2_MISS, true);
          print_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L2_ACCESS, true);
        }
        print_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L3_MISS, true);
        print_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L3_ACCESS, true);
        printf("    " VL KGRN "%11d%%" RESET " IPC\n",
          counter_by_core[i].counter_by_pu[j].ipc);
      }
      else /* Last PU for this core */
      {
        printf("    " LB HL "PU %d\n", counter_by_core[i].counter_by_pu[j].pu_index);
        print_counter(&counter_by_core[i].counter_by_pu[j].CPU_CYCLES, false);
        print_counter(&counter_by_core[i].counter_by_pu[j].INSTRUCTIONS, false);
        print_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L1D_MISS, false);
        print_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L1D_ACCESS, false);
        print_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L1I_MISS, false);
        print_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L1I_ACCESS, false);
        if(cache_L2_activated)
        {
          print_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L2_MISS, false);
          print_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L2_ACCESS, false);
        }
        print_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L3_MISS, false);
        print_counter(&counter_by_core[i].counter_by_pu[j].CACHE_L3_ACCESS, false);
        printf("     " KGRN "%11d%%" RESET " IPC\n",
          counter_by_core[i].counter_by_pu[j].ipc);
      }
    }
    printf("\n");
  }
  /* Print the cache L3 miss/access ratio */
  printf("     L3 ratio " KGRN "%d%%\n" RESET, cache_L3_ratio);
}

/**
 * print_counter
 * print the value of the counter specified in parameter with or without a pipe
 * in front of the line depending on the `pipe` parameter
 *
 * @param:
 *    struct counter* c
 *    bool pipe
 *
 * @return:
 *    void
 */
void print_counter(struct counter* c, bool pipe)
{
  if(pipe)
  {
    printf("    " VL KGRN "%'12" PRIu64 RESET " %-12s (" KCYN "%'" 
      PRIu64 RESET ", " KYEL "%'" PRIu64 RESET ")\n",
      c->value, c->name, c->last_value, c->max_value);
  }
  else
  {
    printf("     " KGRN "%'12" PRIu64 RESET " %-12s (" KCYN "%'" 
      PRIu64 RESET ", " KYEL "%'" PRIu64 RESET ")\n",
      c->value, c->name, c->last_value, c->max_value);
  }
}
