/*
 * Copyright © 2014 David Guyon (david@guyon.me)
 */

#include "performance-tools.h"

int get_nb_of_cpus();
void cache_test();
void check_CPU_version_for_L2_cache_events();

/**
 * init_performance_tools
 * set the number of CPU available on the system, the number of Core and the
 * number of PU by core and initialize all the performances tools depending on
 * the <pid> given at start time of the application (or -1 if not)
 *
 * @param:
 *    hwloc_topology_t topology
 *
 * @return:
 *    void
 */
void init_performance_tools(hwloc_topology_t topology)
{
  local_topology = topology;

  nb_cpu = get_nb_of_cpus();

  /*
   * Check the CPU version to activate or not the L2 cache events
   */
  check_CPU_version_for_L2_cache_events();

  int depth = hwloc_get_type_depth(local_topology, HWLOC_OBJ_CORE);
  nb_core = hwloc_get_nbobjs_by_depth(topology, depth);
  nb_pu_by_core = hwloc_get_obj_by_depth(topology, depth, 0)->arity;

  /* Alloc memory for the structures depending on the nb of cores and PUs */
  counter_by_core = malloc(nb_core * sizeof(struct counter_by_core_t));
  int i;
  for(i = 0 ; i < nb_core ; i++)
  {
    hwloc_obj_t obj = hwloc_get_obj_by_depth(local_topology, depth, i);
    counter_by_core[i].counter_by_pu =
      malloc(nb_pu_by_core * sizeof(struct counter_by_pu_t));
  }

  init_perf_event_counters(lstopo_pid_number);
  init_cpu_usage(lstopo_pid_number);

  cache_test();
}

/**
 * update_performance_tools
 * call the update method of all initialized performance tools
 *
 * @param:
 *    void
 *
 * @return:
 *    void
 */
void update_performance_tools()
{
  update_perf_event_counters();
  update_cpu_usage(lstopo_pid_number);
}

/*******************************************************************************
 * PRIVATES METHODS
 */

/**
 * get_nb_of_cpus
 * return the number of available CPUs in the system
 *
 * @param:
 *    void
 *
 * @return:
 *    int
 */
int get_nb_of_cpus()
{
  int nb = sysconf(_SC_NPROCESSORS_ONLN);
  if (nb < 1) {
    fprintf(stderr, "[e] Could not determine number of CPUs\n");
    exit (EXIT_FAILURE);
  }

  return nb;
}

/**
 * check_CPU_version_for_L2_cache_events
 * initialize `cache_L2_events` to 0 or 1 depending on the configured versions
 * of Intel CPU
 * 
 * Note: feel free to add your version with the correct values in the switch
 *
 * @param:
 *    void
 *
 * @return:
 *    void
 */
void check_CPU_version_for_L2_cache_events()
{
  int a, disp_model, disp_family;
  asm("mov $1, %%eax; " /* a into eax */
      "cpuid;"
      "mov %%eax, %0;"  /* eax into a */
      :"=r"(a) /* output */
      :
      :"%eax","%ebx","%ecx","%edx" /* clobbered register */
     );
  disp_model = ((a >> 4) & 0xf) | (((a >> 16) & 0xf) << 4);
  disp_family = ((a >> 8) & 0xf) | (((a >> 20) & 0xff) << 4);

  if(disp_family<0x10 && disp_model<0x10)
    printf("[i] CPU version: 0%x_0%x\n",disp_family, disp_model);
  else if(disp_family<0x10)
    printf("[i] CPU version: 0%x_%x\n",disp_family, disp_model);
  else if(disp_model<0x10)
    printf("[i] CPU version: %x_0%x\n",disp_family, disp_model);
  else
    printf("[i] CPU version: %x_%x\n",disp_family, disp_model);

  cache_L2_activated = 0;
  switch(disp_family)
  {
    case 0x06:
    {
      switch(disp_model)
      {
        case 0x25:
          cache_L2_activated = 1;
          cache_L2_miss_config = 0xaa24;
          cache_L2_access_config = 0xff24;
          break;
      }
    }
/*  case someFamilyVersion:
 *  {
 *    switch(disp_model)
 *    {
 *      case someModelVersion: cache_L2_activated = true; break;
 *    }
 *  }
 */
  };

  if(cache_L2_activated)  printf("[i] L2 cache events supported\n");
  else                    printf("[i] L2 cache events not supported\n");
}

/**
 * cache_test
 * run a quick soft which generates a lot of cache misses to get a default
 * maximum value for the L3 and L1D and L1I cache misses and accesses
 *
 * @param:
 *    void
 *
 * @return:
 *    void
 */
void cache_test()
{
  printf("[i] Begin cache test\n");
  int i, j, iterations, nb_big_loop = 0;
  uint64_t max_L1D_miss, max_L1D_access;
  uint64_t max_L1I_miss, max_L1I_access;
  uint64_t max_L2_miss, max_L2_access;
  uint64_t sum_L3_miss, sum_L3_access;

  /* Find the L3 cache in the topology and save its size */
  uint64_t L3_size = 0;
  hwloc_obj_t obj;
  for (obj = hwloc_get_obj_by_type(local_topology, HWLOC_OBJ_PU, 0);
       obj;
       obj = obj->parent)
  {
    if ((obj->type == HWLOC_OBJ_CACHE) && (obj->attr->cache.depth == 3))
      L3_size = obj->attr->cache.size;
  }

  printf("[i] L3 cache size: %"PRIu64" bytes\n", L3_size);

  do {
    update_perf_event_counters(); /* Only to reset, values ignored */

    clock_t before = clock();
    int msec = 0;

    /* Snippet of code for the test */
    int *arr;
    int nb = L3_size / sizeof(int);
    arr = (int*) malloc(sizeof(int) * nb);
    int x = 101, random1, random2;

    for(i = 0 ; i < nb ; i++)
      arr[i] = i;
    
    iterations = 0;

    do
    {
      for(i = 0 ; i < 512 ; i++)
      {
        /* generate two random numbers */
        x = (16807*x) % ((unsigned long)(1<<31)-1);
        random1 = (x >> 10) % nb;
        x = (16807*x) % ((unsigned long)(1<<31)-1);
        random2 = (x >> 10) % nb;

        /* swap elements */
        int temp = arr[random1];
        arr[random1] = arr[random2];
        arr[random2] = temp;
      }
      /* End of the snippet of code for the test */

      clock_t difference = clock() - before;
      msec = difference * 1000 / CLOCKS_PER_SEC;
      iterations++;
    }
    while(msec < 1000);

    update_perf_event_counters();

    free(arr);

    max_L1D_miss = 0;
    max_L1D_access = 0;
    max_L1I_miss = 0;
    max_L1I_access = 0;
    max_L2_miss = 0;
    max_L2_access = 0;
    sum_L3_miss = 0;
    sum_L3_access = 0;

    for(i = 0 ; i < nb_core ; i++)
    {
      uint64_t sum_L1D_miss = 0;
      uint64_t sum_L1D_access = 0;
      uint64_t sum_L1I_miss = 0;
      uint64_t sum_L1I_access = 0;
      uint64_t sum_L2_miss = 0;
      uint64_t sum_L2_access = 0;

      for(j = 0 ; j < nb_pu_by_core ; j++)
      {
        sum_L1D_miss   += counter_by_core[i].counter_by_pu[j].CACHE_L1D_MISS.value;
        sum_L1D_access += counter_by_core[i].counter_by_pu[j].CACHE_L1D_ACCESS.value;
        sum_L1I_miss   += counter_by_core[i].counter_by_pu[j].CACHE_L1I_MISS.value;
        sum_L1I_access += counter_by_core[i].counter_by_pu[j].CACHE_L1I_ACCESS.value;
        sum_L2_miss    += counter_by_core[i].counter_by_pu[j].CACHE_L2_MISS.value;
        sum_L2_access  += counter_by_core[i].counter_by_pu[j].CACHE_L2_ACCESS.value;
        sum_L3_miss    += counter_by_core[i].counter_by_pu[j].CACHE_L3_MISS.value;
        sum_L3_access  += counter_by_core[i].counter_by_pu[j].CACHE_L3_ACCESS.value;
      }

      // sum_L1D_miss *= (refresh_time / 10);
      if(sum_L1D_miss > max_L1D_miss)
        max_L1D_miss = sum_L1D_miss;

      // sum_L1D_access *= (refresh_time / 10);
      if(sum_L1D_access > max_L1D_access)
        max_L1D_access = sum_L1D_access;

      // sum_L1I_miss *= (refresh_time / 10);
      if(sum_L1I_miss > max_L1I_miss)
        max_L1I_miss = sum_L1I_miss;

      // sum_L1I_access *= (refresh_time / 10);
      if(sum_L1I_access > max_L1I_access)
        max_L1I_access = sum_L1I_access;

      // sum_L2_miss *= (refresh_time / 10);
      if(sum_L2_miss > max_L2_miss)
        max_L2_miss = sum_L2_miss;

      // sum_L2_access *= (refresh_time / 10);
      if(sum_L2_access > max_L2_access)
        max_L2_access = sum_L2_access;
    }

    // sum_L3_miss   *= (refresh_time / 10);
    // sum_L3_access *= (refresh_time / 10);

    printf("[i] Time taken %d seconds %d milliseconds (%d iterations)\n",
      msec/1000, msec%1000, iterations);

    nb_big_loop++;

    /* To avoid problem if there is no L2 cache events */
    if(cache_L2_activated == 0)
    {
      max_L2_miss = 1;
      max_L2_access = 1;
    }

  /*
   * Check if all counters gave correct values
   * if not, do the test again only if test time does not exceed 250 ms
   */
  } while(
    (sum_L3_miss  == 0 || sum_L3_access  == 0 ||
     max_L2_miss  == 0 || max_L2_access  == 0 ||
     max_L1D_miss == 0 || max_L1D_access == 0 ||
     max_L1I_miss == 0 || max_L1I_access == 0 ) && nb_big_loop < 25
  );

  printf("[i] L3  average cache misses:   %"PRIu64"\n", sum_L3_miss);
  printf("[i] L3  average cache accesses: %"PRIu64"\n", sum_L3_access);
  if(cache_L2_activated)
  {
    printf("[i] L2  average cache misses:   %"PRIu64"\n", max_L2_miss);
    printf("[i] L2  average cache accesses: %"PRIu64"\n", max_L2_access);
  }
  printf("[i] L1D average cache misses:   %"PRIu64"\n", max_L1D_miss);
  printf("[i] L1D average cache accesses: %"PRIu64"\n", max_L1D_access);
  printf("[i] L1I average cache misses:   %"PRIu64"\n", max_L1I_miss);
  printf("[i] L1I average cache accesses: %"PRIu64"\n", max_L1I_access);

  /* Save the maximum values globaly */
  for(i = 0 ; i < nb_core ; i++)
  {
    counter_by_core[i].cache_L1D_max_miss   = max_L1D_miss;
    counter_by_core[i].cache_L1D_max_access = max_L1D_access;
    counter_by_core[i].cache_L1I_max_miss   = max_L1I_miss;
    counter_by_core[i].cache_L1I_max_access = max_L1I_access;
    counter_by_core[i].cache_L2_max_miss    = max_L2_miss;
    counter_by_core[i].cache_L2_max_access  = max_L2_access;
  }
  cache_L3_max_miss   = sum_L3_miss;
  cache_L3_max_access = sum_L3_access;

  update_perf_event_counters(); /* Only to reset, values ignored */

  printf("[i] Cache test: done\n");
}
