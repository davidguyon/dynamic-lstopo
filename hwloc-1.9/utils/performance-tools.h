/*
 * Copyright © 2014 David Guyon (david@guyon.me)
 */

#ifndef UTILS_PERFORMANCE_TOOLS_H
#define UTILS_PERFORMANCE_TOOLS_H

#include <time.h>
#include <math.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <dirent.h>
#include <locale.h>
#include <stropts.h>
#include <inttypes.h>
#include <linux/perf_event.h>

#include "lstopo.h"

/*
 * keywords for colors in printf
 */
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#define RESET "\033[0m"

/*
 * keywords for the ASCII characters to draw boxes
 */
#define RB "\e(0\x6a\e(B" // 188 Right Bottom corner
#define RT "\e(0\x6b\e(B" // 187 Right Top corner
#define LT "\e(0\x6c\e(B" // 201 Left Top cornet
#define LB "\e(0\x6d\e(B" // 200 Left Bottom corner
#define MC "\e(0\x6e\e(B" // 206 Midle Cross
#define HL "\e(0\x71\e(B" // 205 Horizontal Line
#define LC "\e(0\x74\e(B" // 204 Left Cross
#define RC "\e(0\x75\e(B" // 185 Right Cross
#define BC "\e(0\x76\e(B" // 202 Bottom Cross
#define TC "\e(0\x77\e(B" // 203 Top Cross
#define VL "\e(0\x78\e(B" // 186 Vertical Line

/*
 * Boolean implementation
 */
typedef int bool;
#define true 1
#define false 0

#if defined(__x86_64__)
  #define __NR_perf_counter_open  298
#elif defined(__i386__)
  #define __NR_perf_counter_open  336
#else
  #error What syscall???
#endif

int nb_cpu;
int nb_core;
int nb_pu_by_core;
hwloc_topology_t local_topology;

/*
 * Boolean to activate or not L2 cache events depending on the CPU version
 * Initialized in `init_performance_tools()` 
 *
 * if `cache_L2_activated` != 0 make sure the variables `cache_L2_miss_config`
 * and `cache_L2_access_config` are set to the correct value depending on the
 * datasheet of your Intel CPU
 */
int cache_L2_activated;
int cache_L2_miss_config;
int cache_L2_access_config;

/*
 * Structure to store the perf_event counter configuration and to save the
 * current value, the previous value and the maximum value we got so far
 *
 * Update 13/06: `last_read_*` variables used by `read_fd`
 */
struct counter
{
  int file_descriptor;
  int type;
  int config;
  char* name;
  uint64_t value;
  uint64_t max_value;
  uint64_t last_value;
  uint64_t last_read_value;
  uint64_t last_read_time_enabled;
  uint64_t last_read_time_running;
};

/*
 * Structure counter_by_pu_t stores needed data for a specific PU (pu index, ipc
 * cpu usage, cache misses, cache accesses, cpu cycles and instructions)
 */
struct counter_by_pu_t
{
  int ipc;
  int pu_index;
  int cpu_usage;
  struct counter CPU_CYCLES;
  struct counter INSTRUCTIONS;
  struct counter CACHE_L1D_MISS;
  struct counter CACHE_L1D_ACCESS;
  struct counter CACHE_L1I_MISS;
  struct counter CACHE_L1I_ACCESS;
  struct counter CACHE_L2_MISS;
  struct counter CACHE_L2_ACCESS;
  struct counter CACHE_L3_MISS;
  struct counter CACHE_L3_ACCESS;
};

/*
 * Structure counter_by_core_t stores needed data for a specific Core (L1D and
 * L1I cache ratio and a struct counter_by_pu pointer (one for each PU) )
 */
struct counter_by_core_t
{
  int cache_L1D_ratio;
  int cache_L1D_miss;
  int cache_L1D_max_miss;
  int cache_L1D_access;
  int cache_L1D_max_access;
  int cache_L1I_ratio;
  int cache_L1I_miss;
  int cache_L1I_max_miss;
  int cache_L1I_access;
  int cache_L1I_max_access;
  int cache_L2_ratio;
  int cache_L2_miss;
  int cache_L2_max_miss;
  int cache_L2_access;
  int cache_L2_max_access;
  struct counter_by_pu_t * counter_by_pu;
};

/*
 * Struct counter_by_pu pointer (one for each Core)
 */
struct counter_by_core_t * counter_by_core;

/*
 * integer which contains the ratio of miss/access on the cache L3
 * integer which contains the current number of L3 cache misses
 * integer which contains the maximum number of L3 cache misses
 * integer which contains the current number of L3 cache accesses 
 * integer which contains the maximum number of L3 cache accesses 
 */
int cache_L3_ratio;
int cache_L3_miss;
int cache_L3_max_miss;
int cache_L3_access;
int cache_L3_max_access;

/* performance-tools.c */
void init_performance_tools(hwloc_topology_t topology);
void update_performance_tools();

/* performance-tools-perf-event.c */
void init_perf_event_counters(int pid);
void update_perf_event_counters();
void print_counters();

/* performance-tools-cpu-usage.c */
void init_cpu_usage(int pid);
void update_cpu_usage(int pid);

/* performance-tools-drawings.c */
void draw_cpu_usage_on_pu_box(
  struct draw_methods *methods, void *output,
  int x, int y, int width, int max_height,
  int depth, int cpu_number
);
void draw_cache_info_on_cache_box(
  hwloc_obj_t cache_obj, struct draw_methods* methods,
  void* output, int x, int y, 
  int width, int max_height, int depth
);
void update_background_color_on_cache_box(
  hwloc_obj_t cache_obj, int* r, int* g, int* b
);

#endif /* UTILS_PERFORMANCE_TOOLS_H */
